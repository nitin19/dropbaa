@extends('layouts.default')

@section('title', 'Drop Baa')

@section('content')
					<div class="col-sm-8 col-sm-offset-2 formarea">
					          <h1>Reset Password</h1>
					          <div class="white_bg">
					          	<div class="row">
					          		<div class="col-sm-6 col-xs-6 leftborder">
					            		<hr class="borderline">
					          		</div>
					          		<div class="col-sm-6 col-xs-6 rightborder">
					            		<hr class="borderline">
					          		</div>
					        	</div>
					        	<div class="registeration_section formsection">

					                    @if (session('status'))
					                        <div class="alert alert-success">
					                            {{ session('status') }}
					                        </div>
					                    @endif
					                <form class="form-horizontal simple_form login_form resetpwdlkform" id="resetpwdlkform" name="resetpwdlkform" method="POST" action="{{url('forgetpassword/send_password')}}">
                        			{{ csrf_field() }}
                        				<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} inputbox">
                            				<label for="email" class="col-md-4 control-label">E-Mail Address</label>
                            				<div class="col-md-6">
                                				<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" maxlength="121" required>
                                					 @if ($errors->has('email'))
                                   						<span class="help-block">
                                        					<strong>{{ $errors->first('email') }}</strong>
                                    					</span>
                                					@endif 
                            				</div>
                        				</div>
                        				<div class="form-group">
                            				<div class="col-md-6 col-md-offset-4">
                            					<input type="submit" class="btn btn-primary" value="Send Password Reset Link" name="forgotpwdBtn">
                            				</div>
                        				</div>
                    				</form>
					            </div>
					          </div>
					</div>

					<style>
					    body {
					        background: rgba(0, 0, 0, 0) url("{{ url('/public') }}/images/bg-img.jpg") no-repeat scroll center center / cover  ;
					        height: 100vh;
					    }
					</style>

					<script>
					 $(document).ready( function() {

					    $("#resetpwdlkform").validate({
					        rules: {
					                email: {
					                    required: true,
					                    email: true,
					                    maxlength: 120
					                }
					            },
					        messages: {
					                email: {
					                  required: "Please enter email.",
					                  email: "Please enter a valid email.",
					                  maxlength: "Maximum 120 characters allowed."
					                }
					            },
					        submitHandler: function(form) {
					            form.submit();
					          }
					        });

					 });
					</script>
@stop