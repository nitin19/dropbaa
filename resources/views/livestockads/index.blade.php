@extends('layouts.default')

@section('title', 'Drop Baa')

@section('content')
<?php
        $user = Auth::user();    
        $user_id =  Auth::id(); 

        function humanTiming ($time) {
          $time = time() - $time; 
          $time = ($time<1)? 1 : $time;
          $tokens = array (
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'minute',
            1 => 'second'
            );
          foreach ($tokens as $unit => $text) {
            if ($time < $unit) continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
          }
        }
?>
<!--main-section-start-->
<?php
$uid  = Auth::user()->id;
?>
        <main>
            <div class="content_section">
                <div class="profile_banner">
                    <div class="container">
                        <div class="col-sm-4 col-xs-3 profile_img">
                            @if($user->profile_picture!='')
                            <img src="{{ url('/public') }}/uploads/propic/thumbnail/{{ $user->profile_picture }}" class="img-responsive">
                            @else
                            <img src="{{ url('/public') }}/images/dummy.jpg" class="img-responsive">
                            @endif
                        </div>
                        <div class="col-sm-6 col-xs-9 profile_content">
                            <h2>{{ Auth::user()->name }}</h2>
                            <p>{{ Auth::user()->address_line_1 }} {{ Auth::user()->address_line_2 }} {{ Auth::user()->zipcode }}</p>
                            <p><span><b>Email Address:</b>{{ Auth::user()->email }}</span><span><b>Phone Number:</b>{{ Auth::user()->phone }}</span></p>
                            <p class="description"><b>Description</b> <br>{{ Auth::user()->description }}</p>
                        </div>
                        <div class="col-sm-2 col-xs-3 edit_profile profile_content">
                            <p class="edit_profile_btn"><a href="{{ route('users.edit', $user->id) }}">Edit Profile</a></p>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="col-sm-12 content_part tab_view top_spacing">
                        <!--listing-section-start-->
                        <div class="col-sm-12 listing_section nopadding">
                            <div class="listing_header">
                                <h2>My Ads
                                 <p class="add_post_btn" style="text-align: right;"><a href="{{route('livestockads.create')}}">Create Ad</a></p>

                                </h2>

                            </div>
                            @if(count($livestockads)>0)
                            <div class="listing_content">
                                @foreach ($livestockads as $livestock)
                                <div class="col-sm-12 listing_blk">
                                    <div class="col-sm-2 col-xs-3 img_div">
                                       <?php
                                            $postid = $livestock->id;
                                            $postimage = DB::table('postgallery')
                                                        ->where('post_id', $postid)
                                                        ->where('post_type', '=', 'livestockads')
                                                        ->where('status', '=', 1)
                                                        ->where('deleted', '=', 0)->first();
                                                        //$lastQuery = end($postimage);
                                                         //print_r($lastQuery);
                                            ?>
                                           
                                        @if($postimage!='')
                                        <a href="{{ route('livestockads.show', $livestock->id) }}">
                                        <img src="{{ url('/public') }}/uploads/livestockadspics/{{$postimage->file_name}}" class="img-responsive">
                                        </a>
                                         
                                       @else
                                        <a href="#">
                                        <img src="{{ url('/public') }}/images/dummy.jpg" class="img-responsive">
                                        </a>
                                      
                                       @endif
                                    </div>
                                    <div class="col-sm-6 col-xs-7 text_div">
                                        <h3>{{$livestock->lst_ad_title}}</h3>
                                        <p class="more desc_post"><?php
                                            $string = $livestock->lst_description ;
                                            $string1 = (strlen($string) > 5) ? substr($string,0,250).'...' : $string;  
                                            echo $string1;
                                        
                                        ?>
                                        </p>
                                        <p class="price_div"><span>${{$livestock->lst_per_head_per_week}}/P/H/week</span></p>
                                        <p class="posted"><span>Posted:</span> {{ humanTiming(strtotime($livestock->created_at)) }} ago</p>
                                    </div>
                                    <div class="col-sm-3 drop_div">
                                      <a class="edit_btn" href="{{ route('livestockads.edit', $livestock->id) }}">Edit</i></a>
                          {!! Form::open(['method' => 'DELETE','route' => ['livestockads.destroy', $livestock->id],'style'=>'display:inline']) !!}
                         
                            {!! Form::button('Delete', ['class' => 'btn btn-danger','data-toggle'=>'confirmation']) !!}

                          {!! Form::close() !!}
                                    <!-- <a href="{{ route('livestockads.destroy', $livestock->id) }}"></a> -->
                                    <a class="show_btn" href="{{ route('livestockads.show', $livestock->id) }}">View</a>
                                    <div class="map_section nopadding">
                                         <input type="hidden" id="longitude_<?php echo $livestock->id;?>" value="{{$livestock->lst_longitude}}">
                                         <input type="hidden" id="latitude_<?php echo $livestock->id;?>" value="{{$livestock->lst_latitude}}">
                                         <input type="hidden" id="address_<?php echo $livestock->id;?>" value="{{$livestock->lst_address}}">
                                         <input type="hidden" id="zipcode_<?php echo $livestock->id;?>" value="{{$livestock->lst_zipcode}}">
                                         <div id="map_<?php echo $livestock->id;?>" style="width: 100%; height: 200px;"></div> 
                                    </div>
                               <!-- <div class="dropdown">
                                  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action<span class="caret"></span></button>
                                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                                      <li><a href="{{ route('livestockads.edit', $livestock->id) }}">Edit</a></li> -->
                                      <!--<li><a href="{{ route('livestockads.destroy', $livestock->id) }}">Delete</a></li>-->
                                      <!-- <li><a href="{{ route('livestockads.show', $livestock->id) }}">View</a></li>
                                    </ul>
                              </div> -->
                          </div>
                        </div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmECi5THi1W3CHDdmC2bI6q6dDZNRAY-I"></script>
<script type="text/javascript">
/*var numbers = ;
$.each(numbers , function (index, value){
  console.log(index + ':' + value); 
});*/
    function initialize() {
       var longitude = $.trim($('#longitude_<?php echo $livestock->id;?>').val());
       var latitude = $.trim($('#latitude_<?php echo $livestock->id;?>').val());
       var address = $.trim($('#address_<?php echo $livestock->id;?>').val());
       var zipcode = $.trim($('#zipcode_<?php echo $livestock->id;?>').val());
       //lst_address
       //alert(latitude);
       var latlng = new google.maps.LatLng(latitude,longitude);
        //alert(latlng);
        var map = new google.maps.Map(document.getElementById('map_<?php echo $livestock->id;?>'), {
          center: latlng,
          zoom: 10
        });
        var marker = new google.maps.Marker({
          map: map,
          position: latlng,
          draggable: false,
          anchorPoint: new google.maps.Point(0, -29)
       });
        var infowindow = new google.maps.InfoWindow();   
        google.maps.event.addListener(marker, 'click', function() {
          var iwContent = '<div id="iw_container">' +
          '<div class="iw_title"><b>Location</b> : ' + address + ',' + zipcode + '</div></div>';
           //including content to the infowindow
          infowindow.setContent(iwContent);
          // opening the infowindow in the current map and at the current marker location
          infowindow.open(map, marker);
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
                    @endforeach
                </div>
                    @else
                <div class="listing_content">
                    <h4>No Post Found</h4>
                </div>
                    @endif
                <div class="pagination_section">
                <!--listing-section-end-->
                {{ $livestockads->links() }}
                </div>
                </div>
            </div>
        </div>
    </main>
<!--main-section-end-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>
  <script type="text/javascript">

    $(document).ready(function () {        

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.closest('form').submit();

            }

        });   

    });
</script>
<style type="text/css">
.edit_profile_btn {
    background: #fff none repeat scroll 0 0;
    border-radius: 4px;
    float: right;
    padding: 10px 25px;
    text-align: right;
}
.edit_profile_btn > a {
    color: #738c17;
    font-size: 20px;
    font-weight: bold;
}
.edit_btn {
    background: #708712 none repeat scroll 0 0;
    border-radius: 4px;
    color: #fff;
    padding: 6px 25px 8px;
}
.show_btn {
    background: #cfebf6 none repeat scroll 0 0;
    border-radius: 4px;
    padding: 6px 25px 8px;
}
.map_section {
    margin-bottom: 20px;
    margin-top: 15px;
}
a.morelink {
  text-decoration:none;
  outline: none;
}
.morelink {
    color: #708712;
    font-size: 16px;
    font-weight: bold;
}
.morecontent span {
  display: none;
}
.desc_post {
  word-wrap: break-word;
}
</style>
  @stop
