@extends('layouts.default')

@section('title', 'Drop Baa')

@section('content')

        <div class="col-sm-8 col-sm-offset-2 formarea create_post_livestock">
          <h1>Post Your Ad</h1>
          <div class="white_bg">
          <div class="row">
          <div class="col-sm-6 col-xs-6 leftborder">
            <hr class="borderline">
          </div>
          <div class="col-sm-6 col-xs-6 rightborder">
            <hr class="borderline">
          </div>
        </div>
        <div class="registeration_section formsection">

         
          <form class="livestockadform" id="livestockadform" name="livestockadform" method="POST" action="{{ route('livestockads.store') }}" enctype="multipart/form-data">

          {{ csrf_field() }}

            <div class="row">
              <div class="col-sm-12 form-group forminput">
                <label>Agistment Type*</label>
                <select class="form-control" id="lst_agistment_type" name="lst_agistment_type">
                  <option value="">Select Your Agistment Type</option>
                  @foreach ($agistmentcats as $agistcat)
                    <option value="{{ $agistcat->id }}" {{ ( old('lst_agistment_type') ==  $agistcat->id ) ? 'selected' : '' }}>{{ $agistcat->agst_cat_name }}</option>
                  @endforeach

                </select>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 form-group forminput">
                <label>Ad Title*</label>
                <input type="text" class="form-control" id="lst_ad_title" placeholder="Fill Your Title Here" value="{{ old('lst_ad_title') }}" name="lst_ad_title" maxlength="251">
              </div>
            </div>
                <div class="row">
                    <div class="col-sm-12 form-group forminput leftspace">
                      <label>Live Stock:*</label>
                      <div class="checkbox_div">
                        
                        @foreach ($sheepcats as $shepcat)
                        <label class="check_box">{{ $shepcat->shp_cat_name }}
                          <input type="checkbox" name="lst_live_stock[]" id="lst_live_stock_{{ $shepcat->id }}" value="{{ $shepcat->id }}"
                          {{ (is_array(old('lst_live_stock')) && in_array($shepcat->id, old('lst_live_stock'))) ? 'checked' : '' }} >
                          <span class="checkmark"></span>
                        </label>
                        @endforeach


                    </div>
                  </div>
                </div>
                     <div class="row">
              <div class="col-sm-12 form-group forminput">
                <label>Can Supply Fencing:*</label>
                <div class="col-sm-12 radiobtns nopadding">
                  <label class="radiobutton radio-inline">Yes
                  <input type="radio" name="lst_supply_fencing" id="lst_supply_fencing_1" value="Yes" {{ old('lst_supply_fencing')=="Yes" ? 'checked' : '' }}>
                  <span class="checkmark"></span>
                  </label>
                  <label class="radiobutton radio-inline">No
                  <input type="radio" name="lst_supply_fencing" id="lst_supply_fencing_2" value="No" {{ old('lst_supply_fencing')=="No" ? 'checked' : '' }}>
                  <span class="checkmark"></span>
                  </label>
                </div>
              </div>
            </div>
                      <div class="row">
              <div class="col-sm-12 form-group forminput">
                <label>Payment:*</label>
                <div class="col-sm-12 radiobtns nopadding">
                  <label class="radiobutton radio-inline">I Pay
                  <input type="radio" name="lst_payment" id="lst_payment_1" value="I-Pay" {{ old('lst_payment')=="I-Pay" ? 'checked' : '' }}>
                  <span class="checkmark"></span>
                  </label>
                  <label class="radiobutton radio-inline">They Pay
                  <input type="radio" name="lst_payment" id="lst_payment_1" value="They-Pay" {{ old('lst_payment')=="They-Pay" ? 'checked' : '' }}>
                  <span class="checkmark"></span>
                  </label>
                </div>
              </div>
            </div>
                <div class="row">
                    <div class="col-sm-12 col-xs-12 form-group forminput inputdiv currency">
                      <label>Per Head Per Week*<span>Local Currency/Tax Not Included</span></label>
                        <input type="text" class="form-control" id="lst_per_head_per_week" placeholder="$0" value="{{ old('lst_per_head_per_week') }}" name="lst_per_head_per_week" maxlength="10">
                    </div>
                </div>     
                <div class="row">
                    <div class="col-sm-12 form-group forminput">
                      <label>Address*</label>
                        <input type="text" class="form-control" id="lst_address" placeholder="Enter Address" value="{{ old('lst_address') }}" name="lst_address" maxlength="60" onfocusout="GetLocation()">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-xs-12 form-group forminput">
                      <label>Zipcode*</label>
                        <input type="text" class="form-control" id="lst_zipcode" placeholder="Enter Zipcode" value="{{ old('lst_zipcode') }}" name="lst_zipcode" maxlength="10">
                    </div>
                </div>
            <div class="row">
              <div class="col-sm-12 form-group forminput">
                <label>Upload Sheep Images</label>
                <div class="col-sm-12 col-xs-3 browseimage nopadding" id="browse_img">
                    <img src="{{ url('/public') }}/images/default-img.png" class="img-responsive">
                </div>
                <div class="col-sm-12 col-xs-9 nopadding">
                <input type="file" id="lst_pictures" name="lst_pictures[]" multiple accept="image/*" style="display: none;">
                <div class="browse_btn" id="browsebtn">
                  <span>Select Image</span>
                  <p>No File Selected</p>
                  <p class="image_note"><strong>Note:</strong>(Please Upload images this size: 650x300 px.)</p>
                </div>
              </div>
              </div>
            </div>
              <div class="row">
              <div class="col-sm-12 form-group forminput">
                <label>Description*</label>
                <textarea class="form-control" rows="3" placeholder="Write About Sheeps" name="lst_description" id="lst_description" maxlength="5001" minlength="50">{{ old('lst_description') }}</textarea>
              </div>
            </div>

            <input type="hidden" name="lst_latitude" id="lst_latitude" value="">
            <input type="hidden" name="lst_longitude" id="lst_longitude" value="">

            <div class="savebnt text-right">
              <button type="submit" class="btn btn-default">Save</button>
            </div>
         
          </form>

        </div>
      </div>
      </div>


    <style>
      body {
            background: rgba(0, 0, 0, 0) url("{{ url('/public') }}/images/bg-img.jpg") no-repeat scroll center center / cover  ;
            height: 100vh;
          }
    </style>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmECi5THi1W3CHDdmC2bI6q6dDZNRAY-I"></script>
<script type="text/javascript">
        function GetLocation() {
            var geocoder = new google.maps.Geocoder();
            var address = document.getElementById("lst_address").value;
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
                    $('#lst_latitude').val(latitude);
                    $('#lst_longitude').val(longitude);
                } else {
                }
            });
        };
</script>


<script>
 $(document).ready( function() {
    jQuery('#browsebtn').on('click', function() {
             jQuery('#lst_pictures').click();
       });
     
/*  jQuery("#lst_pictures").change(function() {
  var file = this.files[0];
  var imagefile = file.type;
  var imageTypes= ["image/jpeg","image/png","image/jpg"];
    if(imageTypes.indexOf(imagefile) == -1)
    {
      jQuery("#browse_img").html("<span class='msg-error'>Please Select A valid Image File</span><br /><span>Only jpeg, jpg and png Images type allowed</span>");
      return false;
    } else {
      var reader = new FileReader();
      reader.onload = function(e){
        jQuery("#browse_img").html('<img src="' + e.target.result + '"  />');        
      };
      reader.readAsDataURL(this.files[0]);
    }
  });*/

  $(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img style="margin:0 10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#lst_pictures').on('change', function() {
         jQuery('#nofile').empty(); 
          jQuery('#browse_img').empty(); 
        imagesPreview(this, '#browse_img');
    });
});

      $("#livestockadform").validate({
        ignore: [],
        rules: {
                lst_agistment_type: {
                    required: true
                },
                lst_ad_title: {
                    required: true,
                    minlength: 2,
                    maxlength: 250
                },
                'lst_live_stock[]': {
                  required: true
                },
                lst_supply_fencing: {
                    required: true
                },
                lst_payment: {
                    required: true
                },
                lst_per_head_per_week: {
                   required: true,
                   number: true,
                   maxlength: 9
                },
               lst_description: {
                   required: true,
                   maxlength: 5000
               }
            },
        messages: {
                lst_agistment_type: {
                  required: "This is required field."
                },
                lst_ad_title: {
                  required: "This is required field.",
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 250 characters allowed."
                },
                'lst_live_stock[]': {
                  required: "This is required field."
                },
                lst_supply_fencing: {
                  required: "This is required field."
                },
                lst_payment: {
                  required: "This is required field."
                },
                lst_per_head_per_week: {
                  required: "This is required field.",
                  number: "Please enter a valid number.",
                  maxlength: "Maximum 9 characters allowed."
                  },
                lst_description: {
                  required: "This is required field.",
                  maxlength: "Maximum 5000 characters allowed."
                }
            },
        submitHandler: function(form) {
            form.submit();
          }
        });

 });
</script>
  @stop