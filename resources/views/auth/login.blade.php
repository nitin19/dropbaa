@extends('layouts.default')

@section('title', 'Drop Baa')

@section('content')

        <div class="col-sm-6 col-sm-offset-3 formarea">
          <h1>Login</h1>
          <div class="white_bg">
          <div class="row">
          <div class="col-sm-6 col-xs-6 leftborder">
            <hr class="borderline">
          </div>
          <div class="col-sm-6 col-xs-6 rightborder">
            <hr class="borderline">
          </div>
        </div>
        <div class="registeration_section formsection">

            @if (\Session::has('error'))
                        <div class="alert alert-danger alert-dismissable">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                           {!! \Session::get('error') !!}
                        </div>
                    @endif

          <form class="form-horizontal loginform" id="loginform" name="loginform" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group forminput{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="row">
                            <label for="email" class="col-md-12 control-label">E-Mail Address</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" maxlength="121" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        </div>

                        <div class="form-group forminput{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="row">
                            <label for="password" class="col-md-12 control-label">Password</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" name="password" maxlength="16" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        </div>

                        <!-- <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div> -->

                        <div class="form-group">
                            <div class="col-md-12 savebnt nopadding">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>
                                <!--<div class="btn btn-link forgotpwd_link"> <a href="{{ url('/forgetpassword') }}">Forgot Your Password?</a></div>-->
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>

        </div>
      </div>
      </div>
 

    <style>
      body {
            background: rgba(0, 0, 0, 0) url("{{ url('/public') }}/images/bg-img.jpg") no-repeat scroll center center / cover  ;
            height: 100vh;
        }
    </style>

<script>
 $(document).ready( function() {

    $("#loginform").validate({
        rules: {
                email: {
                    required: true,
                    email: true,
                    maxlength: 120
                },
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 15
                }
            },
        messages: {
                email: {
                  required: "Please enter email.",
                  email: "Please enter a valid email.",
                  maxlength: "Maximum 120 characters allowed."
                },
                password: {
                  required: "Please enter password.",
                  minlength: "Minimum 6 characters required.",
                  maxlength: "Maximum 15 characters allowed."
                }   
            },
        submitHandler: function(form) {
            form.submit();
          }
        });

 });
</script>
@stop
