@extends('layouts.default')

@section('title', 'Drop Baa')

@section('content')

        <div class="col-sm-8 col-sm-offset-2 formarea">
          <h1>Sign Up</h1>
          <div class="white_bg">
          <div class="row">
          <div class="col-sm-6 col-xs-6 leftborder">
            <hr class="borderline">
          </div>
          <div class="col-sm-6 col-xs-6 rightborder">
            <hr class="borderline">
          </div>
        </div>
        <div class="registeration_section formsection">

          <form class="registerform" id="registerform" name="registerform" action="{{ route('register') }}" method="post" enctype="multipart/form-data">
           
            {{ csrf_field() }}

            <input type="hidden" name="zipcode" id="zipcode" value="">

            <div class="row">
              <div class="col-sm-12 form-group{{ $errors->has('name') ? ' has-error' : '' }} forminput">
                <label>Your Name*</label>
                <input type="text" class="form-control" id="name" placeholder="Enter Your Full Name" value="{{ old('name') }}" name="name" maxlength="61" required autofocus>
                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
              </div>
            </div>


            <div class="row">
              <div class="col-sm-12 form-group{{ $errors->has('email') ? ' has-error' : '' }} forminput">
                <label>Email Address*</label>
                <input type="email" class="form-control" id="email" placeholder="Enter Your Email" value="{{ old('email') }}" name="email" maxlength="121" required>
                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
              </div>
            </div>


            <div class="row">
             <div class="col-sm-6 form-group{{ $errors->has('password') ? ' has-error' : '' }} forminput leftspace">
                <label>Password*</label>
                <input type="password" class="form-control" id="password" placeholder="Password" value="" name="password" maxlength="16" required>
                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
            </div>


             <div class="col-sm-6 form-group{{ $errors->has('phone') ? ' has-error' : '' }} forminput rightspace">
                <label>Phone Number*</label>
                <input type="text" class="form-control" id="phone" placeholder="Your phone Number" value="{{ old('phone') }}" name="phone" maxlength="13" required>
                 @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
            </div>
          </div>

           <div class="row">
              <div class="col-sm-12 form-group{{ $errors->has('address_line_1') ? ' has-error' : '' }} forminput">
                <label>Address First Line*</label>
                <input type="text" class="form-control" id="address_line_1" placeholder="Address First Line" value="{{ old('address_line_1') }}" name="address_line_1" maxlength="251" required>
                @if ($errors->has('address_line_1'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address_line_1') }}</strong>
                                    </span>
                                @endif
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12 form-group{{ $errors->has('address_line_2') ? ' has-error' : '' }} forminput">
                <label>Address Second Line</label>
                <input type="text" class="form-control" id="address_line_2" placeholder="Address Second Line" value="{{ old('address_line_2') }}" name="address_line_2" maxlength="251">
                 @if ($errors->has('address_line_2'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address_line_2') }}</strong>
                                    </span>
                                @endif
              </div>
            </div>

             <div class="row">
              <div class="col-sm-12 form-group{{ $errors->has('description') ? ' has-error' : '' }} forminput">
                <label>Description</label>
                <textarea class="form-control" rows="3" placeholder="Write About Yourself" name="description" id="description" maxlength="200" minlength="50">{{ old('description') }}</textarea>
                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
              </div>
            </div>
            
             <div class="row">
              <div class="col-sm-12 form-group{{ $errors->has('user_type') ? ' has-error' : '' }} forminput">
                <label>Choose Option*</label>
                <div class="col-sm-12 radiobtns nopadding">
                  <label class="radiobutton radio-inline">I have livestock
                  <input type="radio" name="user_type" value="live_stock_owner" {{ old('user_type') == 'live_stock_owner' ? 'checked' : '' }}>
                  <span class="checkmark"></span>
                  </label>
                  <label class="radiobutton radio-inline">I have land
                  <input type="radio" name="user_type" value="land_owner" {{ old('user_type') == 'land_owner' ? 'checked' : '' }}>
                  <span class="checkmark"></span>
                  </label>

                   @if ($errors->has('user_type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user_type') }}</strong>
                                    </span>
                                @endif
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12 form-group forminput">
                <label>Upload Your Profile Image</label>
                <div class="col-sm-2 col-xs-3 browseimage nopadding" id="browse_img">
                    <img src="{{ url('/public') }}/images/default-img.png" class="img-responsive">
                </div>
                <div class="col-sm-10 col-xs-9">
                <input type="file" id="profile_picture" name="profile_picture" accept="image/*" style="display: none;">
                <div class="browse_btn" id="browsebtn">
                  <span>Browse Image</span>
                  <p>No File Selected</p>
                </div>
              </div>
              </div>
            </div>


            <div class="savebnt text-right">
              <button type="submit" class="btn btn-default">Save</button>
            </div>
          </form>
        </div>
      </div>
      </div>
   

    <style>
      body {
            background: rgba(0, 0, 0, 0) url("{{ url('/public') }}/images/bg-img.jpg") no-repeat scroll center center / cover  ;
            height: 100vh;
        }
    </style>

<script>
 $(document).ready( function() {
 
     $('#browsebtn').on('click', function() {
                 jQuery('#profile_picture').click();
           });
     
    $("#profile_picture").change(function() {
          var file = this.files[0];
          var imagefile = file.type;
          var imageTypes= ["image/jpeg","image/png","image/jpg"];
    if(imageTypes.indexOf(imagefile) == -1) {
      jQuery("#browse_img").html("<span class='msg-error'>Please Select A valid Image File</span><br /><span>Only jpeg, jpg and png Images type allowed</span>");
      return false;
      } else {
      var reader = new FileReader();
      reader.onload = function(e){
        jQuery("#browse_img").html('<img src="' + e.target.result + '"  />');        
      };
      reader.readAsDataURL(this.files[0]);
    }
  });

    $.validator.addMethod("alphaLetterNumber", function(value, element) {
     return this.optional(element) || value == value.match(/^[a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
    });

     $.validator.addMethod("alphaLetterNumberSpace", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
    });

    $.validator.addMethod("alphaLetter", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
    });

    $("#registerform").validate({
        rules: {
                name: {
                    required: true,
                    alphaLetter: true,
                    minlength: 2,
                    maxlength: 60
                },
                email: {
                    required: true,
                    email: true,
                    maxlength: 120
                    /*remote: {
                        url: "{{ url('/register/checkuseremail') }}",
                        type: "GET",
                    }*/
                },
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 15
                },
               phone: {
                   required: true,
                   number: true,
                   minlength: 10,
                   maxlength: 12
               },
               address_line_1: {
                   required: true,
                   maxlength: 250
               },
               user_type: {
                   required: true
               }
            },
        messages: {
              name: {
                  required: "Please enter name.",
                  alphaLetter: "Please enter a valid name.",
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 60 characters allowed."
                },
                email: {
                  required: "Please enter email.",
                  email: "Please enter a valid email.",
                  maxlength: "Maximum 120 characters allowed."
                 // remote: "Email already in use."
                },
                password: {
                  required: "Please enter password.",
                  minlength: "Minimum 6 characters required.",
                  maxlength: "Maximum 15 characters allowed."
                },    
                phone: {
                  required: "Please enter number.",
                  number: "Please enter a valid number.",
                  minlength: "Minimum 10 characters required.",
                  maxlength: "Maximum 12 characters allowed."
                  },
                address_line_1: {
                  required: "Please enter address.",
                  maxlength: "Maximum 250 characters allowed."
                },
                user_type: {
                  required: "Please choose option."
                }
            },
        submitHandler: function(form) {
            form.submit();
          }
        });


 });
</script>
@stop
