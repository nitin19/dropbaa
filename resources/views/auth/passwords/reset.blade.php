@extends('layouts.default')

@section('title', 'Drop Baa')

@section('content')


        <div class="col-sm-8 col-sm-offset-2 formarea">
          <h1>Reset Password</h1>
          <div class="white_bg">
          <div class="row">
          <div class="col-sm-6 col-xs-6 leftborder">
            <hr class="borderline">
          </div>
          <div class="col-sm-6 col-xs-6 rightborder">
            <hr class="borderline">
          </div>
        </div>
        <div class="registeration_section formsection">

          <form class="form-horizontal resetpwdform" id="resetpwdform" name="resetpwdform" method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" maxlength="121" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" maxlength="16" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                                <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" maxlength="16" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Reset Password
                                </button>
                            </div>
                        </div>
                    </form>

        </div>
      </div>
      </div>
   

    <style>
      body {
            background: rgba(0, 0, 0, 0) url("{{ url('/public') }}/images/bg-img.jpg") no-repeat scroll center center / cover  ;
            height: 100vh;
        }
    </style>

<script>
 $(document).ready( function() {

    $("#resetpwdform").validate({
        rules: {
                email: {
                    required: true,
                    email: true,
                    maxlength: 120
                },
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 15
                },
                password_confirmation: {
                    required: true,
                    minlength: 6,
                    maxlength: 15,
                    equalTo: "#password"
                }
            },
        messages: {
                email: {
                  required: "Please enter email.",
                  email: "Please enter a valid email.",
                  maxlength: "Maximum 120 characters allowed."
                },
                password: {
                  required: "Please enter password.",
                  minlength: "Minimum 6 characters required.",
                  maxlength: "Maximum 15 characters allowed."
                },
                password_confirmation: {
                  required: "Please enter confirm password.",
                  minlength: "Minimum 6 characters required.",
                  maxlength: "Maximum 15 characters allowed.",
                  equalTo: "Password and confirm password must be same."
                }    
            },
        submitHandler: function(form) {
            form.submit();
          }
        });

 });
</script>
@stop
