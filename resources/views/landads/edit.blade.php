@extends('layouts.default')

@section('title', 'DropBaa')

@section('title', 'Drop Baa')

@section('content')
<?php 

// echo '<pre>';
// print_r($post);
// echo '</pre>';
if($landad->lnd_i_want!=''){
$shipcat = explode(',', $landad->lnd_i_want);
} else {
$shipcat = array();    
}
?>

        <div class="col-sm-8 col-sm-offset-2 formarea edit_profile_section_land">
          <h1>Edit Your Ad</h1>
          <div class="white_bg">
          <div class="row">
          <div class="col-sm-6 col-xs-6 leftborder">
            <hr class="borderline">
          </div>
          <div class="col-sm-6 col-xs-6 rightborder">
            <hr class="borderline">
          </div>
        </div>
        <div class="registeration_section formsection">
          
          <!-- <form class="landadform" id="landadform" name="landadform" method="post" action="" enctype="multipart/form-data" action=""> -->
            {{ Form::model($landad, array('route' => array('landads.update', $landad->id), 'id' => 'landadform', 'method' => 'PUT', 'files' => true)) }}

           

            <div class="row">
              <div class="col-sm-12 form-group forminput">
                <label>Agistment Type*</label>
                <select class="form-control" id="lnd_agistment_type" name="lnd_agistment_type">
                  <option value="">Select Your Agistment Type</option>
                  @foreach ($agistmentcats as $agistcat)
                    <option value="{{ $agistcat->id }}" {{ ( old('lnd_agistment_type', $landad->lnd_agistment_type) ==  $agistcat->id ) ? 'selected' : '' }}> {{ $agistcat->agst_cat_name }} </option>
                  @endforeach

                
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 form-group forminput">
                <label>Ad Title*</label>
                <input type="text" class="form-control" id="lnd_ad_title" placeholder="Fill Your Title Here" name="lnd_ad_title" maxlength="251" value="{{ old('lnd_ad_title', $landad->lnd_ad_title)}}">
              </div>
            </div>
            <div class="row">
             <div class="col-sm-12 form-group forminput leftspace">
                <label>Land Type*</label>
                <div class="radiobtns">

                 @foreach ($landcats as $lndcat)   
                  <label class="radiobutton radio-inline">{{ $lndcat->lnd_cat_name }}
                  <input type="radio" name="lnd_land_type" id="lnd_land_type" value="{{ $lndcat->id }}"
                   {{ ( old('lnd_land_type', $landad->lnd_land_type) ==  $lndcat->id ) ? 'checked' : '' }}>
                  <span class="checkmark"></span>
                  </label>
                  @endforeach

              

                </div>
            </div>
          </div>
                <div class="row">
                    <div class="col-sm-12 form-group forminput leftspace">
                      <label>I Want*</label>
                      <div class="checkbox_div">

                        @foreach ($sheepcats as $shepcat)

                        <label class="check_box">{{ $shepcat->shp_cat_name }}
                          <input type="checkbox" name="lnd_i_want[]" id="lnd_i_want_{{ $shepcat->id }}" value="{{ $shepcat->id }}" @if(in_array($shepcat->id, $shipcat)){{'checked'}} @endif >
                          <span class="checkmark"></span>
                        </label>
                        @endforeach


                    </div>
                  </div>
                </div>
                <div class="row">
              <div class="col-sm-12 form-group forminput">
                <label>Payment:*</label>
                <div class="col-sm-12 radiobtns nopadding">
                  <label class="radiobutton radio-inline">I Pay
                  <input type="radio" name="lnd_payment" id="lnd_payment_1" value="I-Pay" {{ old('lnd_payment', $landad->lnd_payment)=="I-Pay" ? 'checked' : '' }}>
                  <span class="checkmark"></span>
                  </label>
                  <label class="radiobutton radio-inline">They Pay
                  <input type="radio" name="lnd_payment" id="lnd_payment_1" value="They-Pay" {{ old('lnd_payment', $landad->lnd_payment)=="They-Pay" ? 'checked' : '' }}>
                  <span class="checkmark"></span>
                  </label>
                </div>
              </div>
            </div>
                <div class="row">
             <div class="col-sm-6 col-xs-6 form-group forminput leftspace inputdiv land">
                <label>Land(In hectares)*<span>Unit in Hectare</span></label>
                <input type="text" class="form-control" id="lnd_in_hectares" placeholder=""  name="lnd_in_hectares" maxlength="10" value="{{ old('lnd_in_hectares', $landad->lnd_in_hectares)}}">
            </div>
            <div class="col-sm-6 col-xs-6 form-group forminput rightspace inputdiv currency">
                <label>Per Head Per Week*<span>Local Currency/Tax Not Included</span></label>
                 <input type="text" class="form-control" id="lnd_per_head_per_week" placeholder="$0" value="{{ old('lnd_per_head_per_week', $landad->lnd_per_head_per_week) }}" name="lnd_per_head_per_week" maxlength="10">
            </div>
          </div>
          <div class="row">
              <div class="col-sm-12 form-group forminput">
                <label>Address*</label>
                <input type="text" class="form-control" id="lnd_address" placeholder="Fill Your Address" name="lnd_address"  value="{{ old('lnd_address', $landad->lnd_address)}}" onfocusout="GetLocation()">
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 form-group forminput">
                <label>Zipcode*</label>
                <input type="text" class="form-control" id="lnd_zipcode" placeholder="Fill Your zipcode" name="lnd_zipcode"  value="{{ old('lnd_zipcode', $landad->lnd_zipcode)}}">
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 form-group forminput">
                <label>Upload Landads Post Images</label>
                 <div class="col-sm-12 col-xs-3 browseimage nopadding" id="browse_img">
                  @if(count($postgalleries)>0)
                    @foreach ($postgalleries as $postgallery)
                    <div class="col-sm-2 single_img" id="prv_{{$postgallery->id}}">
                      <a href="javascript:void(0)" class="delete_btn" id="{{$postgallery->id}}" title="Delete">X</a>
                      <img src="{{ url('/public') }}/uploads/landadspics/{{$postgallery->file_name}}" class="img-responsive" style="margin:0 10px">
                    </div>
                    @endforeach
                    @else
                    <img src="{{ url('/public') }}/images/dummy.jpg" class="img-responsive">
                    @endif
                </div>
                <div class="col-sm-12 col-xs-9 nopadding">
                <input type="file" id="lnd_pictures" name="lnd_pictures[]" multiple accept="image/*" style="display: none;">
                <div class="browse_btn" id="browsebtn">
                  <span>Select Images</span>
                  <p id="nofile">No File Selected</p>
                  <p class="image_note"><strong>Note:</strong>(Please Upload images this size: 650x300 px.)</p>
                </div>
              </div>
              </div>
            </div>
              <div class="row">
              <div class="col-sm-12 form-group forminput">
                <label>Description*</label>
                <textarea class="form-control" rows="3" placeholder="Write About Yourself" name="lnd_description" id="lnd_description" maxlength="5001" minlength="50">{{ old('lnd_description', $landad->lnd_description)}}</textarea>
              </div>
            </div>
            <input type="hidden" name="lnd_latitude" id="lnd_latitude" value="{{ old('lnd_latitude', $landad->lnd_latitude) }}">
            <input type="hidden" name="lnd_longitude" id="lnd_longitude" value="{{ old('lnd_longitude', $landad->lnd_longitude) }}">
            <div class="savebnt text-right">
              <button type="submit" class="btn btn-default">Save</button>
            </div>
          <!-- </form> -->
          {{ Form::close() }}
        </div>
      </div>
      </div>
    </div>

    <style>
      body {
             background: rgba(0, 0, 0, 0) url("{{ url('/public') }}/images/bg-img.jpg") no-repeat scroll center center / cover  ;
             height: 100vh;
        }
  .single_img > a {
    background: #ff0000 none repeat scroll 0 0;
    border-radius: 14px;
    color: #fff;
    left: 23px;
    padding: 0 5px 0 6px;
    position: absolute;
    top: -9px;
}
    </style>
<script>
  jQuery(document).ready(function(){
    jQuery(".delete_btn").on('click', function(e){ 
    if(confirm("Are you sure you want to delete")){
        var galleryid = jQuery(this).attr('id');
      if(galleryid!='') {
      jQuery.ajax({
            type : 'GET',
            url  : "{{ url('/landads') }}/{{ $landad->id }}/edit/delete_image/{galleryid}",
            data:{ 'galleryid': galleryid},
            success :  function(resp) {
               $('#prv_'+galleryid).remove();
                }
            });
           }
         } else {
         return false;
         }
       });
       
    
       
  });
</script>    
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmECi5THi1W3CHDdmC2bI6q6dDZNRAY-I"></script>
<script type="text/javascript">
        function GetLocation() {
            var geocoder = new google.maps.Geocoder();
            var address = document.getElementById("lnd_address").value;
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
                    $('#lnd_latitude').val(latitude);
                    $('#lnd_longitude').val(longitude);
                } else {
                }
            });
        };
</script>
<script>
 $(document).ready( function() {
 
 jQuery('#browsebtn').on('click', function() {
             jQuery('#lnd_pictures').click();
       });
     
 //  jQuery("#lnd_pictures").change(function() {
 //  var file = this.files[0];
 //  var imagefile = file.type;
 //  var imageTypes= ["image/jpeg","image/png","image/jpg"];
 //    if(imageTypes.indexOf(imagefile) == -1)
 //    {
 //      jQuery("#browse_img").html("<span class='msg-error'>Please Select A valid Image File</span><br /><span>Only jpeg, jpg and png Images type allowed</span>");
 //      return false;
 //    } else {
 //      var reader = new FileReader();
 //      reader.onload = function(e){
 //        jQuery("#browse_img").html('<img src="' + e.target.result + '"  />');  
 //        jQuery('#nofile').empty();      
 //      };
 //      reader.readAsDataURL(this.files[0]);
 //    }
 //  });

 $(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img style="margin:0 10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#lnd_pictures').on('change', function() {
         jQuery('#nofile').empty(); 
          jQuery('#browse_img').empty(); 
        imagesPreview(this, '#browse_img');
    });
});

   $("#landadform").validate({
        ignore: [],
        rules: {
                lnd_agistment_type: {
                    required: true
                },
                lnd_ad_title: {
                    required: true,
                    minlength: 2,
                    maxlength: 250
                },
                lnd_land_type: {
                    required: true
                },
                'lnd_i_want[]': {
                  required: true
                },
                lnd_in_hectares: {
                    required: true,
                    number: true,
                    maxlength: 9
                },
                lnd_per_head_per_week: {
                   required: true,
                   number: true,
                   maxlength: 9
                },
               lnd_description: {
                   required: true,
                   maxlength: 5000
               },
               lnd_address: {
                   required: true,
               },
               lnd_zipcode: {
                   required: true,
               }
            },
        messages: {
                lnd_agistment_type: {
                  required: "This is required field."
                },
                lnd_ad_title: {
                  required: "This is required field.",
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 250 characters allowed."
                },
                lnd_land_type: {
                  required: "This is required field."
                },
                'lnd_i_want[]': {
                  required: "This is required field."
                },
                lnd_in_hectares: {
                  required: "This is required field.",
                  number: "Please enter a valid number.",
                  maxlength: "Maximum 9 characters allowed."
                },
                lnd_per_head_per_week: {
                  required: "This is required field.",
                  number: "Please enter a valid number.",
                  maxlength: "Maximum 9 characters allowed."
                  },
                lnd_description: {
                  required: "This is required field.",
                  maxlength: "Maximum 5000 characters allowed."
                },
                lnd_address: {
                  required: "This is required field.",
                },
                lnd_zipcode: {
                  required: "This is required field.",
                },
            },
        submitHandler: function(form) {
            form.submit();
          }
        });

 });
</script>
   
 @stop