@extends('layouts.default')
@section('title', 'Drop Baa')
@section('content')
 <?php
        $user = Auth::user();    
        $user_id =  Auth::id(); 

        // echo '<pre>';
        // print_r($posts);
        // echo '</pre>';

        function humanTiming ($time) {
          $time = time() - $time; 
          $time = ($time<1)? 1 : $time;
          $tokens = array (
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'minute',
            1 => 'second'
            );
          foreach ($tokens as $unit => $text) {
            if ($time < $unit) continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
          }
        }
        ?>

 <!--main-section-start-->
        <main>
            <div class="content_section">
                <div class="profile_banner">
                    <div class="container">
                        <div class="col-sm-4 col-xs-3 profile_img">
                            @if($user->profile_picture!='')
                            <img src="{{ url('/public') }}/uploads/propic/thumbnail/{{ $user->profile_picture }}" class="img-responsive">
                            @else
                            <img src="{{ url('/public') }}/images/dummy.jpg" class="img-responsive">
                            @endif
                        </div>
                        <div class="col-sm-6 col-xs-6 profile_content">
                            <h2>{{ Auth::user()->name }}</h2>
                            <p>{{ Auth::user()->address_line_1 }} {{ Auth::user()->address_line_2 }} {{ Auth::user()->zipcode }}</p>
                            <p><span><b>Email Address:</b>{{ Auth::user()->email }}</span><span><b>Phone Number:</b>{{ Auth::user()->phone }}</span></p>
                            <p class="description"><b>Description</b> <br>{{ Auth::user()->description }}</p>
                        </div>
                         <div class="col-sm-2 col-xs-3 edit_profile profile_content">
                             <p class="edit_profile_btn"><a href="{{ route('users.edit', $user->id) }}">Edit Profile</a></p>
                            <!-- <a href="{{ url('/edit', $user_id) }}">Edit Profile</a> -->
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="col-sm-12 content_part tab_view top_spacing">
                        <!--listing-section-start-->
                        <div class="col-sm-12 listing_section nopadding">
                            <div class="listing_header">
                                <h2>My Ads 
                                 <p class="add_post_btn" style="text-align: right;"><a href="{{url('landads/create')}}">Create Ad</a></p>
                                </h2>
                            </div>
                            @if(count($landads)>0)
                            <div class="listing_content">
                                @foreach ($landads as $landad)
                                <div class="col-sm-12 listing_blk">
                                    <div class="col-sm-2 col-xs-3 img_div">
                                       <?php
                                            $postid = $landad->id;
                                            $postimage = DB::table('postgallery')
                                                        ->where('post_id', $postid)
                                                        ->where('post_type', '=', 'landads')
                                                        ->where('status', '=', 1)
                                                        ->where('deleted', '=', 0)->first();
                                                        //$lastQuery = end($postimage);
                                                         //print_r($lastQuery);
                                            ?>
                                           
                                        @if($postimage!='')
                                        <a href="{{ route('landads.show', $landad->id) }}">
                                        <img src="{{ url('/public') }}/uploads/landadspics/{{$postimage->file_name}}" class="img-responsive">
                                        </a>
                                         
                                       @else
                                        <a href="#">
                                        <img src="{{ url('/public') }}/images/dummy.jpg" class="img-responsive">
                                        </a>
                                      
                                       @endif
                                    </div>
                                    <!-- <div class="col-sm-2 col-xs-3 img_div">
                                       
                                        <a href="#"><img src="{{ url('/public') }}/images/dummy.jpg" class="img-responsive"></a>
                                      
                                    </div> -->
                                    <div class="col-sm-6 col-xs-7 text_div">
                                        <h3><a href="{{ route('landads.show', $landad->id) }}">{{$landad->lnd_ad_title}}</a></h3>
                                        <p class="desc_post">
                                        <?php
                                            $string = $landad->lnd_description;
                                            $string1 = (strlen($string) > 5) ? substr($string,0,250).'...' : $string;  
                                            echo $string1;
                                        
                                        ?>
                                        </p>
                                        <p class="price_div"><span>${{ number_format($landad->lnd_per_head_per_week,2) }}/P/H/week</span></p>
                                        <p class="posted"><span>Posted:</span> {{ humanTiming(strtotime($landad->created_at)) }} ago</p>
                                    </div>
                                    <div class="col-sm-3 drop_div">
                                         <a class="edit_btn" href="{{ route('landads.edit', $landad->id) }}">Edit</i></a>
                            {!! Form::open(['method' => 'DELETE','route' => ['landads.destroy', $landad->id],'style'=>'display:inline']) !!}
                         
                            {!! Form::button('Delete', ['class' => 'btn btn-danger','data-toggle'=>'confirmation']) !!}

                            {!! Form::close() !!}
                                      <a class="show_btn" href="{{ route('landads.show', $landad->id) }}">View</a>
                                      <div class="map_section nopadding">
                                         <input type="hidden" id="longitude_<?php echo $landad->id;?>" value="{{$landad->lnd_longitude}}">
                                         <input type="hidden" id="latitude_<?php echo $landad->id;?>" value="{{$landad->lnd_latitude}}">
                                         <input type="hidden" id="address_<?php echo $landad->id;?>" value="{{$landad->lnd_address}}">
                                         <input type="hidden" id="zipcode_<?php echo $landad->id;?>" value="{{$landad->lnd_zipcode}}">
                                         <div id="map_<?php echo $landad->id;?>" style="width: 100%; height: 200px;"></div> 
                                      </div>
                                     <!-- <div class="dropdown">
                                        <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action<span class="caret"></span></button>
                                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                                            <li><a href="{{ route('landads.edit', $landad->id) }}">Edit</a></li>
                                            <li><a href="{{ route('landads.destroy', $landad->id) }}">Delete</a></li>
                                            <li><a href="{{ route('landads.show', $landad->id) }}">View</a></li>
                                        </ul>
                                    </div> -->
                                </div>
                            </div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmECi5THi1W3CHDdmC2bI6q6dDZNRAY-I"></script>
<script type="text/javascript">
function initialize() {
       var longitude = $.trim($('#longitude_<?php echo $landad->id;?>').val());
       var latitude = $.trim($('#latitude_<?php echo $landad->id;?>').val());
       var address = $.trim($('#address_<?php echo $landad->id;?>').val());
       var zipcode = $.trim($('#zipcode_<?php echo $landad->id;?>').val());
       //lst_address
       //alert(latitude);
       var latlng = new google.maps.LatLng(latitude,longitude);
        //alert(latlng);
        var map = new google.maps.Map(document.getElementById('map_<?php echo $landad->id;?>'), {
          center: latlng,
          zoom: 10
        });
        var marker = new google.maps.Marker({
          map: map,
          position: latlng,
          draggable: false,
          anchorPoint: new google.maps.Point(0, -29)
       });
        var infowindow = new google.maps.InfoWindow();   
        google.maps.event.addListener(marker, 'click', function() {
          var iwContent = '<div id="iw_container">' +
          '<div class="iw_title"><b>Location</b> : ' + address + ',' + zipcode + '</div></div>';
           //including content to the infowindow
          infowindow.setContent(iwContent);
          // opening the infowindow in the current map and at the current marker location
          infowindow.open(map, marker);
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
                                @endforeach
                        </div>
                        @else
                         <div class="listing_content">
                            <h4>No Post Found</h4>
                         </div>
                        @endif
                        <div class="pagination_section">
                        <!--listing-section-end-->
                        {{ $landads->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!--main-section-end-->
        <style type="text/css">

       .map_section {
           margin-bottom: 20px;
           margin-top: 15px;
       }
       .edit_profile_btn {
          background: #fff none repeat scroll 0 0;
          border-radius: 4px;
          float: right;
          padding: 10px 25px;
          text-align: right;
        }
       .edit_profile_btn > a {
          color: #738c17;
          font-size: 20px;
          font-weight: bold;
        }
       .edit_btn {
          background: #708712 none repeat scroll 0 0;
          border-radius: 4px;
          color: #fff;
          padding: 6px 25px 8px;
       }
       .show_btn {
          background: #cfebf6 none repeat scroll 0 0;
          border-radius: 4px;
          padding: 6px 25px 8px;
       }
       .text_div a {
          color: #708712;
       }
       a.morelink {
          text-decoration:none;
          outline: none;
       }
       .morelink {
          font-size: 16px;
          font-weight: bold;
       }
       .morecontent span {
          display: none;
       }
       .desc_post {
         word-wrap: break-word;
       }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>
  <script type="text/javascript">

    $(document).ready(function () {        

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.closest('form').submit();

            }

        });   

    });
</script>
  @stop