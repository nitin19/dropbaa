@extends('layouts.default')
@section('title', 'Drop Baa')
@section('content')

<?php
// echo '<pre>';
// print_r($postgalleries);
// echo '</pre>';
?>

<!--main-section-start-->
        <main>
            <div class="content_section">
                <div class="container">
                    <div class="col-sm-12 tab_view">
                        <div class="single_view">
                            <div class="col-sm-8 singleview_left">
                                <h3>{{$landad->lnd_ad_title}}</h3>
                                <span>${{ number_format($landad->lnd_per_head_per_week,2) }}/P/H/week</span>
                                <div class="slider_area">

                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">


  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    @if(count($postgalleries)>0)
    <?php $firstslide = $postgalleries[0]->id; ?>
    @foreach ($postgalleries as $postgallery)
    <div class="item <?php if($postgallery->id==$firstslide){ echo 'active';} ?>
        ">
      <img src="{{ url('/public') }}/uploads/landadspics/{{$postgallery->file_name}}" style="width:100%">
      <div class="carousel-caption">
      </div>
    </div>
    @endforeach
    @else
    <div class="item active">
      <img src="{{ url('/public') }}/images/slide-img1.jpg" style="width:100%">
      <div class="carousel-caption">
      </div>
    </div>
    @endif
  </div>

  <!-- Controls -->
  @if(count($postgalleries)>1)
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
  @else
  @endif
</div>
                                   
                                    <div class="col-sm-12 singleview_content nopadding">
                                        <p><b>Job Description:</b><br>{{$landad->lnd_description}}</p>
                                        <p class="land_type"><span>Land Type:</span> 
                                            <?php
                                            $landcatid = $landad->lnd_land_type;
                                            $catName = DB::table('landcat')->where('id', $landcatid)->first();
                                            echo $catName->lnd_cat_name;
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 singleview_right">
                                <h3>{{$landad->lnd_address}}, {{$landad->lnd_zipcode}}</h3>
                                   <input type="hidden" id="longitude" value="{{$landad->lnd_longitude}}"> 
                                   <input type="hidden" id="latitude" value="{{$landad->lnd_latitude}}">
                                   <input type="hidden" id="address" value="{{$landad->lnd_address}}">
                                   <input type="hidden" id="zipcode" value="{{$landad->lnd_zipcode}}">
                                   <div id="map" style="width: 100%; height: 300px;"></div> 
                                <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d60999571.3051211!2d95.33175625209142!3d-21.067934178984416!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2b2bfd076787c5df%3A0x538267a1955b1352!2sAustralia!5e0!3m2!1sen!2sin!4v1521778916950" width="100%" height="200" frameborder="0" style="border:2px solid #759c2e" allowfullscreen></iframe> -->
                                <div class="contact_details">
                                    <h2>Contact Detail</h2>
                                    <p>Name<span>{{ Auth::user()->name }}</span></p>
                                    <p>Email Address<span><a href="mailto:{{ Auth::user()->email }}">{{ Auth::user()->email }}</a></span></p>
                                    <p>Phone Number<span><a href="tel:+{{ Auth::user()->phone }}">{{ Auth::user()->phone }}</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!--main-section-end-->
        <script>
        function openModal() {
            document.getElementById('myModal').style.display = "block";
        }

        function closeModal() {
            document.getElementById('myModal').style.display = "none";
        }

        var slideIndex = 1;
        showSlides(slideIndex);

        function plusSlides(n) {
            showSlides(slideIndex += n);
        }

        function currentSlide(n) {
            showSlides(slideIndex = n);
        }

        function showSlides(n) {
            var i;
            var slides = document.getElementsByClassName("mySlides");
            var dots = document.getElementsByClassName("demo");
            var captionText = document.getElementById("caption");
            if (n > slides.length) {
                slideIndex = 1
            }
            if (n < 1) {
                slideIndex = slides.length
            }
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
            }
            slides[slideIndex - 1].style.display = "block";
            dots[slideIndex - 1].className += " active";
            captionText.innerHTML = dots[slideIndex - 1].alt;
        }
    </script>
  <style type="text/css">
      body {
            background: rgba(0, 0, 0, 0) url("{{ url('/public') }}/images/inner_bgimg.jpg") no-repeat scroll center center / cover;
            height: 100vh;
        }
      .carousel-control{
        background: none !important;
      }
  </style>
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmECi5THi1W3CHDdmC2bI6q6dDZNRAY-I"></script>
<script type="text/javascript">
function initialize() {
       var longitude = $.trim($('#longitude').val());
       var latitude = $.trim($('#latitude').val());
       var address = $.trim($('#address').val());
       var zipcode = $.trim($('#zipcode').val());
       //lst_address
       //alert(latitude);
       var latlng = new google.maps.LatLng(latitude,longitude);
        //alert(latlng);
        var map = new google.maps.Map(document.getElementById('map'), {
          center: latlng,
          zoom: 13
        });
        var marker = new google.maps.Marker({
          map: map,
          position: latlng,
          draggable: false,
          anchorPoint: new google.maps.Point(0, -29)
       });
        var infowindow = new google.maps.InfoWindow();   
        google.maps.event.addListener(marker, 'click', function() {
          var iwContent = '<div id="iw_container">' +
          '<div class="iw_title"><b>Location</b> : ' + address + ',' + zipcode + '</div></div>';
          // including content to the infowindow
          infowindow.setContent(iwContent);
          // opening the infowindow in the current map and at the current marker location
          infowindow.open(map, marker);
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>

  @stop
