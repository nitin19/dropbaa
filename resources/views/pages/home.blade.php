@extends('layouts.default2')

@section('title', 'Drop Baa')

@section('content')



         <main class="mobile_section">
            <div class="content_section ">
                <div class="container mobile_section_container">
                    <div class="mobile_homecontent">
                        <div class="col-sm-6 mobhome_leftside">
                            <div class="mobview_btn"><a href="{{route('searchland.index')}}">View Ads <br/>
                     Offering Livestock</a></div>
                        </div>
                        <div class="col-sm-6 mobhome_rightside">
                            <div class="mobview_btn"><a href="{{route('searchlivestocks.index')}}">View Ads <br/>
                    Offering Land</a></div>                           
                        </div>
                    </div>
                    
                </div>
            </div>
        </main>
        <main class="tab_section">
            <div class="content_section ">
                <div class="container tab_section_container">

                    <div class="home_content first_tab_imgae">
                        <div class="col-sm-6 home_leftside first_image_tab_section">
                            <h1><a href="{{route('searchland.index')}}">View Ads <br/>
                     Offering Livestock</a></h1>
                        </div>
                        <div class="col-sm-6 home_rightside third_image_tab_section">
                            <h1><a href="{{route('searchlivestocks.index')}}">View Ads <br/>
                    Offering Land</a></h1>                         
                        </div>
                    </div>
                    
                </div>
            </div>
        </main>
     
        <main>
            <div class="content_section desktop_section">
                <div class="container">

                    <div class="col-sm-12 home_content">
                        <div class="col-sm-6 home_leftside">
                            <h1><a href="{{route('searchland.index')}}">View Ads <br/>
                     Offering Livestock</a></h1>
                        </div>
                        <div class="col-sm-6 home_rightside">
                            <h1><a href="{{route('searchlivestocks.index')}}">View Ads <br/>
                    Offering Land</a></h1>
                        </div>
                    </div>
                </div>
            </div>
        </main>
       

    <style>
.home_leftside a {
    border: 2px solid #000;
    color: #000;
    float: left;
    font-size: 21px;
    letter-spacing: 2px;
    padding: 1px 6px;
    text-transform: uppercase;
    width: 100%;
}
.home_rightside a {
    border: 2px solid #000;
    color: #000;
    float: left;
    font-size: 21px;
    letter-spacing: 2px;
    padding: 1px 6px;
    text-transform: uppercase;
    width: 100%;
}
    @media (min-width:1600px) and (max-width: 1920px) {
body{background-position: center;}
.home_leftside > h1 {
    left: 10%;
    position: relative;
    top: 280px;
}
.home_rightside > h1 {
    position: relative;
    top: 280px;left: 30%;
}
    }
       @media (min-width:1400px) and (max-width: 1600px) {
body{background-position: center;}
    }
    @media screen and (min-width:768px) and (max-width:991px){
      /*body {
               background: rgba(0, 0, 0, 0) url("http://dropbaa.net/public/images/home-bg.jpg") repeat scroll 0 0 / cover ;
                height: 100vh;
       }*/
       .logo_div.second_tab_section {
          background: #fff none repeat scroll 0 0;
          border-radius: 64px;
          margin-left: -35px;
          padding: 0;
          z-index: 2147483647;
      }
    
      body {
          background: #F8FEDA url("http://dropbaa.net/public/images/dropbaa-newimg.jpg") repeat scroll 0 0 / cover ;
          height: 100vh;background-position:center;
      }
      .menu_after_login {
          width: 100%;
      }
       .desktop_section {
          display:none;
       }
       .tab_section {
        display: block;
       }
       .mobile_section {
          display:none;
       }
       .loginbtn_border {
          width: 203px !important;
       }
       .registerbtn a {
          width: 184px !important;
          font-size: 13px !important;
          line-height: 17px;
       }
    .login_btn_section {
    margin-left: 40%;
    padding: 0;
    width: 60%;
}
       .register_btn_section {
          width: 75% !important;
       } 
       .logo_div {
          top: 25%;
       }
       .home_leftside a , .home_rightside a{font-size: 15px;padding: 6px;}
       .btnborder > a{font-size: 16px;}
    }
    @media screen and (min-width:992px){
     body {
    background: #F8FEDA url("http://dropbaa.net/public/images/dropbaa-newimg.jpg") no-repeat scroll 0 0 / cover ;
    height: 100vh;background-position: center;
    }
       .desktop_section {
          display:block;
       }
       .tab_section {
        display: none !important;
       }
       .mobile_section {
          display:none;
       }
    }
    @media screen and (max-width:767px){
    body{
        background: none !important;
        height: 0;
    }
    .mobile_section {
        top:0px;
    }
    .logo_div.second_section a img {
        width: 100%;
    }
    .first_image_section a, .third_image_section a {
    	background: #fff none repeat scroll 0 0;
    	border-radius: 60px;
    	color: #7fbd33;
    	float: left;
    	padding: 17px 34px;
    }
    .registerbtn a {
	font-size: 13px;
	line-height: 17px;
	padding: 5px 22px;
     }
     .loginbtn_border > a {
        padding: 5px 45px;
     }
    .mobile_section_container{
        padding-left:0px;
        padding-right:0px;
    }
   .home_leftside > h1 {
	font-size: 16.88px;
	left: 30px;
	line-height: 20px;
	position: absolute;
    }
    .third_image_section > h1 {
        font-size: 16.88px;
        left: 30px;
        line-height: 20px;
        position: absolute;
        top: 42px;
        font-family: Cheeronsta Bold;
        text-align: center;
    }

    .home_leftside img {
        padding-right: 0px;
        width: 100%;
    }
    .third_image_section img {
        padding-right: 0px !important;
        width: 100%;
    }

    .logo_div {
	position: inherit;
	top: auto;
	width: 100%;
	left: auto;
    }

     .mobile_section {
        display:block;
     }
     .desktop_section {
          display:none;
     }
     .tab_section {
        display: none !important;
     }
    }
    @media (max-width: 767px) {
      .btnborder > a{font-size: 18px;}
      .login_btn_section{width:50%; padding-left: 0;}
      .mobhome_leftside {
    padding-left: 0;
}
.mobhome_rightside{padding-left: 0px;}
.mobview_btn > a {
    border: 2px solid #000009;
    color: #000009;
    float: left;
    font-size: 16px;
    line-height: 19px;
    margin-top: 5px;
    text-align: center;
    width: 100%;
}
.mobile_homecontent {
    float: left;
    left: 50%;
    position: relative;
    top: -110px;
    width: 50%;
}
    }
    </style>

@stop