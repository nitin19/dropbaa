@extends('layouts.default2')

@section('title', 'Drop Baa')

@section('content')



         <main class="mobile_section">
            <div class="content_section ">
                <div class="container mobile_section_container">

                    <div class="home_content first_imgae">
                        <div class="col-sm-12 home_leftside first_image_section">
                            <h1><a href="{{route('searchland.index')}}">I have Live Stock and <br/>
                     Looking  for Land</a></h1>
                        </div>
                        <div class="logo_div second_section">
                             <a href="https://winebaa.com/" target="_blank"><img src="{{ url('/public') }}/images/winebaa_logo.png" class="img-responsive"></a>
                        </div>
                        <div class="col-sm-12 third_image_section">
                            <h1><a href="{{route('searchlivestocks.index')}}">I have Land and<br/>
                    Looking  for Live Stock</a></h1>                           
                        </div>
                    </div>
                    
                </div>
            </div>
        </main>


        <main class="tab_section">
            <div class="content_section ">
                <div class="container tab_section_container">

                    <div class="home_content first_tab_imgae">
                        <div class="col-sm-6 home_leftside first_image_tab_section">
                            <h1><a href="{{route('searchland.index')}}">I have Live Stock and <br/>
                     Looking  for Land</a></h1>
                            <img src="{{ url('/public') }}/images/leftside_img.PNG">
                        </div>
                        <div class="logo_div second_tab_section">
                             <a href="https://winebaa.com/" target="_blank"><img src="{{ url('/public') }}/images/winebaa_logo.png" class="img-responsive"></a>
                        </div>
                        <div class="col-sm-6 home_rightside third_image_tab_section">
                            <h1><a href="{{route('searchlivestocks.index')}}">I have Land and<br/>
                    Looking  for Live Stock</a></h1> 
                            <img src="{{ url('/public') }}/images/rightside_img.png">                          
                        </div>
                    </div>
                    
                </div>
            </div>
        </main>
     
        <main>
            <div class="content_section desktop_section">
                <div class="container">

                    <div class="col-sm-12 home_content">
                        <div class="col-sm-6 home_leftside">
                            <h1><a href="{{route('searchland.index')}}">I have Live Stock and <br/>
                     Looking  for Land</a></h1>
                            <img src="{{ url('/public') }}/images/leftside_img.PNG">
                        </div>
                        <div class="col-sm-6 home_rightside">
                            <h1><a href="{{route('searchlivestocks.index')}}">I have Land and<br/>
                    Looking  for Live Stock</a></h1>

                            
                            <img src="{{ url('/public') }}/images/rightside_img.png">
                        </div>
                    </div>
                    <div class="logo_div">
                        <a href="https://winebaa.com/" target="_blank"><img src="{{ url('/public') }}/images/winebaa_logo.png" class="img-responsive"></a>
                    </div>
                </div>
            </div>
        </main>
       

    <style>
    @media (min-width:1600px) and (max-width: 1920px) {
body{background-position: center !important;}
    }
       @media (min-width:1400px) and (max-width: 1600px) {
body{background-position: center !important;}
    }
    @media screen and (min-width:768px) and (max-width:991px){
      /*body {
               background: rgba(0, 0, 0, 0) url("http://dropbaa.net/public/images/home-bg.jpg") repeat scroll 0 0 / cover ;
                height: 100vh;
       }*/
       .logo_div.second_tab_section {
          background: #fff none repeat scroll 0 0;
          border-radius: 64px;
          margin-left: -35px;
          padding: 0;
          z-index: 2147483647;
      }
      .first_image_tab_section {
          background-image: url("http://dropbaa.net/public/images/image_1.png");
          left: -31px;
      }
      .third_image_tab_section {
          background-image: url("http://dropbaa.net/public/images/image_2.png");
          left: 30px;
          padding-bottom: 51px;
          padding-top: 5px;
          position: relative;
      }
      body {
          background: rgba(0, 0, 0, 0) url("http://dropbaa.net/public/images/bg-img.jpg") repeat scroll 0 0 / cover ;
          height: 100vh;
      }
      .menu_after_login {
          width: 100%;
      }
       .desktop_section {
          display:none;
       }
       .tab_section {
        display: block;
       }
       .mobile_section {
          display:none;
       }
       .loginbtn_border {
          width: 203px !important;
       }
       .registerbtn a {
          width: 184px !important;
          font-size: 13px !important;
          line-height: 17px;
       }
       .login_btn_section {
          width: 25% !important;
       }
       .register_btn_section {
          width: 75% !important;
       } 
       .logo_div {
          top: 25%;
       }
    }
    @media screen and (min-width:992px){
     body {
    background: rgba(0, 0, 0, 0) url("http://dropbaa.net/public/images/home-bg.jpg") no-repeat scroll 0 0 / cover ;
    height: 100vh;
    }
       .desktop_section {
          display:block;
       }
       .tab_section {
        display: none !important;
       }
       .mobile_section {
          display:none;
       }
    }
    @media screen and (max-width:767px){
    body{
        background: none !important;
        height: 0;
    }
    .mobile_section {
        top:0px;
    }
    .logo_div.second_section a img {
        width: 100%;
    }
    .first_image_section a, .third_image_section a {
    	background: #fff none repeat scroll 0 0;
    	border-radius: 60px;
    	color: #7fbd33;
    	float: left;
    	padding: 17px 34px;
    }
    .registerbtn a {
	font-size: 13px;
	line-height: 17px;
	padding: 5px 22px;
     }
     .loginbtn_border > a {
        padding: 5px 45px;
     }
     .text-right {
        width: 100%;
     }
    .first_image_section {
       background: rgba(0, 0, 0, 0) url("{{ url('/public') }}/images/image_1.png") no-repeat scroll center center / cover;
       padding: 133px;
    
    }
    .third_image_section {
       background: rgba(0, 0, 0, 0) url("{{ url('/public') }}/images/image_2.png") no-repeat scroll center center / cover;
       padding: 133px;
    
    }
    
    .mobile_section_container{
        padding-left:0px;
        padding-right:0px;
    }
   .home_leftside > h1 {
	font-size: 16.88px;
	left: 30px;
	line-height: 20px;
	position: absolute;
    }
    .third_image_section > h1 {
        font-size: 16.88px;
        left: 30px;
        line-height: 20px;
        position: absolute;
        top: 42px;
        font-family: Cheeronsta Bold;
        text-align: center;
    }

    .home_leftside img {
        padding-right: 0px;
        width: 100%;
    }
    .third_image_section img {
        padding-right: 0px !important;
        width: 100%;
    }

    .logo_div {
	position: inherit;
	top: auto;
	width: 100%;
	left: auto;
    }

     .mobile_section {
        display:block;
     }
     .desktop_section {
          display:none;
     }
     .tab_section {
        display: none !important;
     }
    }
    </style>

@stop