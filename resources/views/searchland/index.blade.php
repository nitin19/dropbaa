@extends('layouts.default')

@section('title', 'Drop Baa')

@section('content')

         <main>
            <div class="content_section">
                <div class="container">
                    <div class="col-sm-12 content_part tab_view">

            <form class="search_form" name="searchform" id="searchform" method="GET" action="{{ route('searchland.index') }}">  

                        <div class="col-sm-12 searchbar_bg">
                            <h3>Search For Land</h3>
                           
                                <div class="col-sm-10 col-xs-8 nopadding">
                                    <div class="form-group">
                    <input type="text" class="form-control" placeholder="Enter Your Keyword" name="keyword" id="keyword" value="{{ $keyword }}">
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-4 nopadding">
                                    <button type="submit" class="btn btn-default">Search</button>
                                </div>
                         
                        </div>

                        <div class="col-sm-12 filters_div nopadding">
                            <div class="row">

                                <div class="col-sm-3 filter_input leftspace">
                                    <select name="land_type" id="land_type" onchange="this.form.submit()">
                                            <option value="">Select Land Type</option>
                                            <option value="">All</option>
                                            @foreach ($landcats as $landcat)
                                            <option value="{{ $landcat->id }}" {{ ( $land_type ==  $landcat->id ) ? 'selected' : '' }}>{{ $landcat->lnd_cat_name }}</option>
                                             @endforeach
                                    </select>
                                </div>

                                <div class="col-sm-3 filter_input rightspace leftspace">
                                    <select name="live_type" id="live_type" onchange="this.form.submit()">
                                            <option value="">Select Livestock Type</option>
                                            <option value="">All</option>
                                            @foreach ($sheepcats as $sheepcat)
                                            <option value="{{ $sheepcat->id }}" {{ ( $live_type ==  $sheepcat->id ) ? 'selected' : '' }}>{{ $sheepcat->shp_cat_name }}</option>
                                            @endforeach
                                    </select>
                                </div>
                                 <div class="col-sm-3 filter_input rightspace leftspace">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="location" placeholder="Enter Location or postcode" name="location" value="{{ $location }}">
                                    </div>
                                </div> 

                                <!-- <div class="col-sm-4 filter_input rightspace leftspace">
                                    <div class="form-group price_block">
                                        <input type="text" class="form-control" id="price" placeholder="Price/week" name="price" value="{{ $price }}">
                                    </div>
                                </div> -->
                                
                                <div class="col-sm-3 filter_input rightspace">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="distance" placeholder="Distance" name="distance" value="{{ $distance }}" onkeyup="GetLocation()">
                                            <!--<input type="hidden" class="form-control" id="curtlatitude" placeholder="Distance" name="latitude" value="">
                                            <input type="hidden" class="form-control" id="curtlangitude" placeholder="Distance" name="langitude" value="">-->
                                            
                                            <!--<div class="input-group-addon">KMS.</div>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            
                       </form>
                       
        <?php
        function humanTiming ($time) {
          $time = time() - $time; 
          $time = ($time<1)? 1 : $time;
          $tokens = array (
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'minute',
            1 => 'second'
            );
          foreach ($tokens as $unit => $text) {
            if ($time < $unit) continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
          }
        }
     ?>

                        <!--listing-section-start-->
                        <div class="col-sm-12 listing_section nopadding">
                            <div class="listing_header">
                               
                           </div>
                            <div class="listing_content">

                                 @if( count($landads) > 0 )
                                 @foreach ($landads as $landad)
                                <div class="col-sm-12 listing_blk">
                                    <div class="col-sm-2 col-xs-3 img_div">
                                        <?php
                                            $postid = $landad->id;
                                            $postimage = DB::table('postgallery')
                                                        ->where('post_id', $postid)
                                                        ->where('post_type', '=', 'landads')
                                                        ->where('status', '=', 1)
                                                        ->where('deleted', '=', 0)->first();
                                            ?>  

                                 @if($postimage!='')          
                        <a href="{{ route('searchland.show', $landad->id) }}"><img src="{{ url('/public') }}/uploads/landadspics/{{ $postimage->file_name }}" class="img-responsive"></a>
                                    @else
                        <a href="{{ route('searchland.show', $landad->id) }}"><img src="{{ url('/public') }}/images/listing_img.JPG" class="img-responsive"></a>
                                     @endif                     

                                    </div>
                                    <div class="col-sm-8 col-xs-7 text_div">
                                        <h3>{{ $landad->lnd_ad_title }}</h3>
                                        <p>
                                           <?php
                                            $string = $landad->lnd_description;
                                            $string1 = (strlen($string) > 5) ? substr($string,0,250).'...' : $string;  
                                            echo $string1;
                                        ?>
                                        </p>
                                        <?php
                                            $land_type_id = $landad->lnd_land_type;
                                            $land_cat_data = DB::table('landcat')
                                                        ->where('id', $land_type_id)
                                                        ->where('status', '=', 1)
                                                        ->where('deleted', '=', 0)->first();
                                            
                                          $sheep_id = $landad->lnd_i_want;
                                          $sheepids = explode(',',$sheep_id);
                                          ?>
                                         
                                        <p class="posted"><span class="heading">PREFERED LIVESTOCK: </span>
                                          @foreach ($sheepids as $sheepcatid)
                                          <?php 
                                      
                                          $sheepcatdata = DB::table('sheepcat')
                                                        ->where('id', $sheepcatid)
                                                        ->where('status', '=', 1)
                                                        ->where('deleted', '=', 0)->first();
                                          ?>
                                          {{ $loop->first ? '' : ', ' }}
                                          {{ $sheepcatdata->shp_cat_name }}
                                          @endforeach
                                          
                                        </p>
                                         
                                         <p class="posted"><span>LAND TYPE: </span>{{ $land_cat_data->lnd_cat_name }}</p>
                                         @if($landad->lnd_payment =='I-Pay')
                                        <p class="posted"><span>PAYMENT: </span> They-Pay ${{ $landad->lnd_per_head_per_week }}/P/H/week</p>
                                        @elseif($landad->lnd_payment =='They-Pay')
                                        <p class="posted"><span>PAYMENT: </span> I-Pay  ${{ $landad->lnd_per_head_per_week }}/P/H/week</p>
                                        @else
                                        <p class="posted"><span>PAYMENT: </span> ${{ $landad->lnd_per_head_per_week }}/P/H/week</p>
                                        @endif
                                        <?php
                                        $land_assign_id = $landad->lnd_agistment_type;
                                        $land_assign_data = DB::table('agistmentcat')
                                                        ->where('id', $land_assign_id)
                                                        ->where('status', '=', 1)
                                                        ->where('deleted', '=', 0)->first();
                                        ?>
                                        <p class="posted"><span>TERM: </span>{{ $land_assign_data->agst_cat_name }}</p>
                                        <p class="posted"><span>Posted:</span> {{ humanTiming(strtotime($landad->created_at)) }} ago</p>

                                    </div>
                                    <div class="col-sm-2 col-xs-2 listing_btn">
                                        <a href="{{ route('searchland.show', $landad->id) }}" class="viewbtn">View Detail</a>
                                        <!-- <a href="#" class="savebtn">Save</a> -->
                                    </div>
                                </div>

                                 @endforeach

                                 @else
                                     <div class="col-sm-12 listing_section nopadding">
                                 	<div class="no_found_data">
                                    		<h2 class="no_found_text">No Result Found...</h2>
                                 	</div>
                           	     </div>

				 @endif 

                            </div>
                        </div>

                        <div class="pagination_section">
                         
                            
                            <!-- <div class="pagination">
                                <div class="pagination_numbers">
                                    
                                </div>
                            </div> -->
                        </div>
                        <!--listing-section-end-->
                    </div>
                </div>
            </div>
        </main>
    

    <style>
        body {
            background: rgba(0, 0, 0, 0) url("{{ url('/public') }}/images/inner_bgimg.jpg") no-repeat scroll center center / cover;
            height: 100vh;
        }
        a.morelink {
          text-decoration:none;
          outline: none;
       }
       .morelink {
          font-size: 16px;
          font-weight: bold;
       }
       .morecontent span {
          display: none;
       }
       .input-group {
         width:100%;
       }
       .no_found_text {
          font-size: 24px;
          padding: 50px;
          text-align: center;
        }
    </style>

<script>
 $(document).ready( function() {
 
   $("#location").focusout(function(){
        $("#searchform").submit();
       });
   $("#price").focusout(function(){
        $("#searchform").submit();
       });
   $("#distance").focusout(function(){
        $("#searchform").submit();
       });
    });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmECi5THi1W3CHDdmC2bI6q6dDZNRAY-I"></script>
<!--<script type="text/javascript">
function GetLocation() {
   if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
   }else {
    alert('Geo Location feature is not supported in this browser.');
   } 
 }
  function showPosition(p) {
      var lat = p.coords.latitude;
      var lang = p.coords.longitude;
      alert(lat);
      alert(lang);
      $("#curtlatitude").val(lat);
      $("#curtlangitude").val(lang); 
   }
</script>-->
<script>
$(document).ready(function() {
  var showChar = 450;
  var ellipsestext = "...";
  var moretext = "Read More";
  var lesstext = "Read Less";
  $('.more').each(function() {
    var content = $(this).html();

    if(content.length > showChar) {

      var c = content.substr(0, showChar);
      var h = content.substr(showChar-0, content.length - showChar);

      var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

      $(this).html(html);
    }

  });
  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });
});
</script>

 @stop