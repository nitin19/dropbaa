@extends('layouts.default')

@section('title', 'Drop Baa')

@section('content')

        <div class="col-sm-8 col-sm-offset-2 formarea edit_profile_section_user">
          <h1>Edit Your Profile</h1>
          <div class="white_bg">
          <div class="row">
          <div class="col-sm-6 col-xs-6 leftborder">
            <hr class="borderline">
          </div>
          <div class="col-sm-6 col-xs-6 rightborder">
            <hr class="borderline">
          </div>
        </div>
        <div class="registeration_section formsection">
          <form class="registerform" id="registerform" action="{{ route('users.update', $user->id) }}" method="POST" enctype="multipart/form-data">
          
           {{ csrf_field() }}          

          {{ method_field('PUT') }}
            <div class="row">
              <div class="col-sm-12 form-group forminput">
                <label>Your Name</label>
                <input type="text" class="form-control" id="name" placeholder="Enter Your Full Name" value="{{ $user->name }}" name="name">
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 form-group forminput">
                <label>Email Address</label>
                <input type="email" class="form-control" id="email" placeholder="Enter Your Email" value="{{ $user->email }}" name="email" readonly="">
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 form-group forminput">
                 <label>Phone Number</label>
                <input type="text" class="form-control" id="phone" placeholder="Your phone Number" value="{{ $user->phone }}" name="phone">
              </div>
            </div>
           
           <div class="row">
              <div class="col-sm-12 form-group forminput">
                <label>Address First Line</label>
                <input type="text" class="form-control" id="address_line_1" placeholder="Address First Line" value="{{ $user->address_line_1 }}" name="address_line_1">
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 form-group forminput">
                <label>Address Second Line</label>
                <input type="text" class="form-control" id="address_line_2" placeholder="Address Second Line" value="{{ $user->address_line_2 }}" name="address_line_2">
              </div>
            </div>
             <div class="row">
              <div class="col-sm-12 form-group forminput">
                <label>Description</label>
                <textarea class="form-control" name="description" rows="3" placeholder="Write About Yourself" maxlength="200" minlength="50">{{ $user->description }}</textarea>
              </div>
            </div>
            <!--  <div class="row">
              <div class="col-sm-12 form-group forminput">
                <label>Choose Option</label>
                <div class="col-sm-12 radiobtns nopadding">
                  <label class="radiobutton radio-inline">I have livestock
                  <input type="radio" name="radio" >
                  <span class="checkmark"></span>
                  </label>
                  <label class="radiobutton radio-inline">I have land
                  <input type="radio" name="radio">
                  <span class="checkmark"></span>
                  </label>
                </div>
              </div>
            </div> -->
            <div class="row">
              <div class="col-sm-12 form-group forminput">
                <label>Upload Your Profile Image</label>
                <div class="col-sm-2 col-xs-3 browseimage nopadding" id="browse_img">
                  @if($user->profile_picture!='')
                    <img src="{{ url('/public') }}/uploads/propic/thumbnail/{{$user->profile_picture}}" class="img-responsive">
                  @else
                    <img src="{{ url('/public') }}/images/dummy.jpg" class="img-responsive">
                  @endif
                </div>
                <div class="col-sm-10 col-xs-9">
                <input type="file" id="profile_picture" name="profile_picture" accept="image/*" style="display: none;">
                <div class="browse_btn" id="browsebtn">
                  <span>Browse Image</span>
                  <p>No File Selected</p>
                </div>
              </div>
              </div>
            </div>
            <div class="savebnt text-right">
              <button type="submit" class="btn btn-default">Save</button>
            </div>
          </form>
        </div>
      </div>
      </div>
    </div>
    <style>
      body {
    background: rgba(0, 0, 0, 0) url("{{ url('/public') }}/images/bg-img.jpg") no-repeat scroll center center / cover  ;
    
    height: 100vh;
}
    </style>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!--     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
 <!--    <script src="js/bootstrap.min.js"></script> -->
  </body>
</html>
<script>
 $(document).ready(function() {
 
            jQuery('#browsebtn').on('click', function() {
                 jQuery('#profile_picture').click();
            });
            jQuery("#profile_picture").change(function() {
                 var file = this.files[0];
                 var imagefile = file.type;
                 var imageTypes= ["image/jpeg","image/png","image/jpg"];
                if(imageTypes.indexOf(imagefile) == -1)
                 {
                      jQuery("#browse_img").html("<span class='msg-error'>Please Select A valid Image File</span><br /><span>Only jpeg, jpg and png Images type allowed</span>");
                      return false;
                 } else {
                 var reader = new FileReader();
                 reader.onload = function(e){
                    jQuery("#browse_img").html('<img src="' + e.target.result + '"  />');        
                 };
                  reader.readAsDataURL(this.files[0]);
                }
            });


  $.validator.addMethod("alphaLetterNumber", function(value, element) {
     return this.optional(element) || value == value.match(/^[a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
    });

     $.validator.addMethod("alphaLetterNumberSpace", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
    });

    $.validator.addMethod("alphaLetter", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
    });

    $("#registerform").validate({
        rules: {
                name: {
                    required: true,
                    alphaLetter: true,
                    minlength: 2,
                    maxlength: 60
                },
               phone: {
                   required: true,
                   number: true,
                   minlength: 10,
                   maxlength: 12
               },
               address_line_1: {
                   required: true,
                   maxlength: 250
               }
               /*user_type: {
                   required: true
               }*/
            },
        messages: {
              name: {
                  required: "Please enter name.",
                  alphaLetter: "Please enter a valid name.",
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 60 characters allowed."
                },
              phone: {
                  required: "Please enter number.",
                  number: "Please enter a valid number.",
                  minlength: "Minimum 10 characters required.",
                  maxlength: "Maximum 12 characters allowed."
                  },
              address_line_1: {
                  required: "Please enter address.",
                  maxlength: "Maximum 250 characters allowed."
                }
                /*user_type: {
                  required: "Please choose option."
                }*/
            },
        submitHandler: function(form) {
            form.submit();
          }
        });

 });
    </script>

      @stop