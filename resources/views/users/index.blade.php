@extends('layouts.default')

@section('title', 'WineBaa')

@section('content')

<section id="posts" class="parallax-section">

<div class="container">
        <div class="row">
       
           <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Posts</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-success" href="{{ route('posts.create') }}"> Create New Post</a>

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif


    <table class="table table-bordered">

        <tr>

            <th>No</th>

            <th>Title</th>

            <th>Description</th>
            
             <th>Image</th>

            <th width="280px">Action</th>

        </tr>

    @foreach ($livestockads as $key => $post)

    <tr>

        <td>{{ ++$i }}</td>

        <td>{{ $post->lst_ad_title }}</td>

        <td>{{ $post->lst_description }}</td>
        
         <td><img src="{{ url('public/uploads/postsimages/'.$post->lst_pictures) }}" width="100" height="60" /></td>

        <td>

            <a class="btn btn-info" href="{{ route('livestockads.show',$post->id) }}">Show</a>

            <a class="btn btn-primary" href="{{ route('livestockads.edit',$post->id) }}">Edit</a>

            {!! Form::open(['method' => 'DELETE','route' => ['livestockads.destroy', $post->id],'style'=>'display:inline']) !!}

            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}

            {!! Form::close() !!}

        </td>

    </tr>

    @endforeach

    </table>


    {!! $livestockads->render() !!} 
            
        
        </div>
      </div>
		
		</section>
<style>
      body {
    background: rgba(0, 0, 0, 0) url("{{ url('/public') }}/images/bg-img.jpg") no-repeat scroll center center / cover  ;
    height: 100vh;
}
    </style>
  @stop