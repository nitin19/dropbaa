 <header class="">
            <div class="container-fluid">
                <div class="col-sm-4 col-xs-12 logo_img logodiv_img">
                  <a href="https://winebaa.com/" target="_blank"><img src="{{ url('/public') }}/images/winebaa_logo.png" class="img-responsive" target="_blank"></a>
                </div>
                <div class="col-sm-4 dropbaa-img text-center">
                   <a href="#" target="_blank"><img src="{{ url('/public') }}/images/dropbaa-text.png" class="dropbaa-text"><br/><img src="{{ url('/public') }}/images/dropba-logo.png" class="img-responsive dropbaaimg" target="_blank"></a>
                </div>
                <div class="col-sm-4 col-xs-12 text-right mobpadding">
                    <div class="col-sm-12">
                    @if(Auth::check())
                     <?php
                          $user = Auth::user();    
                          $user_id =  Auth::id(); 
                     ?>
                     
                     <div class="dropdown menu_after_login">
                      @if($user->profile_picture!='')
                        <button class="btn btn-primary dropdown-toggle dropdown_menu" type="button" data-toggle="dropdown"><span class="user_name_title">Welcome, {{ Auth::user()->name }}</span><img src="{{ url('/public') }}/uploads/propic/thumbnail/{{ $user->profile_picture }}" class="img-circle"></button>
                        
                      @else
                        <button class="btn btn-primary dropdown-toggle dropdown_menu" type="button" data-toggle="dropdown"><span class="user_name_title">Welcome, {{ Auth::user()->name }}</span><img src="{{ url('/public') }}/images/dummy.jpg" class="img-circle">
                        <span class="caret"></span></button>
                      @endif
                          <ul class="dropdown-menu dropdown_menu_list">
                                  @if($user->user_type == 'live_stock_owner' ) 
                                  <li><a href="{{ route('livestockads.index') }}">{{ Auth::user()->name }}</a></li>
                                  @else
                                  <li><a href="{{ route('landads.index') }}">{{ Auth::user()->name }}</a></li>
                                  @endif
                                  <li><a href="{{ route('users.edit', $user->id) }}">Edit Profile</a></li>
                                  <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                          {{ csrf_field() }}
                                         </form>
                                      </a>
                                  </li>
                            </ul>
                       </div>
                     @else
                      <div class="col-sm-12 col-xs-12 text-right nopadding">
                        <div class="row">
                        <div class="col-sm-6 col-xs-12 col-sm-offset-6 login_btn_section">
                           <div class="btnborder">
                                <a href="{{ route('register') }}">Create Free<br/> AD </a>
                            </div>
                            <div class="btnborder">
                                <a href="{{ route('login') }}">Login</a>
                            </div>
                        </div>
                      </div>
                       @endif
                      </div>
                    </div>
                </div>
            </div>
        </header>
        <style type="text/css">
          .dropdown_menu {
              font-size: 18px;
              font-weight: bold;
          }
          .menu_after_login {
              width: auto;
          }
          .user_name_title {
              color: #708712 !important;
              padding-right: 10px;
          }
          .dropbaa-text {
    	      left: 30px;
              margin: auto;
              position: relative;
              width: 200px;
          }
	  .dropbaaimg {
    	      left: 40px;
              margin: auto;
              position: relative;
              width: 300px;
          }
          .home_leftside a {
	      border: 2px solid #000;
	      color: #000;
	      float: left;
	      font-size: 21px;
	      letter-spacing: 6px;
	      text-transform: uppercase;
	      width: 100%;
          }
	  .home_rightside a {
	      border: 2px solid #000;
	      color: #000;
	      float: left;
	      font-size: 21px;
	      letter-spacing: 6px;
	      text-transform: uppercase;
	      width: 100%;
	  }
	  .menu_after_login img {
	      padding-left: 10px;
	      width: 60px;
	  }
	  .menu_after_login .caret {
	      color: #000;
	  }
	  ul.dropdown_menu_list {
	      left: 35%;
	  }
	  @media (max-width:767px) {
	      .dropbaa-text {
	           left: 0px;
	           width: 150px;
	      }
	      .dropbaaimg {
		   left: 40px;
		   width: 150px;
	      }
          }
          .menu_after_login img {
    		height: 60px;
    		padding-left: 0px;
    		width: 60px;
	  }
        </style>
        