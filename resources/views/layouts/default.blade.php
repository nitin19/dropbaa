<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>@yield('title')</title>
    <link rel="shortcut icon" href="{{ url('/public') }}/favicon.ico" type="image/x-icon">
    <!-- Bootstrap -->
    <link href="{{ url('/public') }}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ url('/public') }}/css/style.css" rel="stylesheet">
    <link href="{{ url('/public') }}/css/responsive.css" rel="stylesheet">
    <link href="{{ url('/public') }}/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="{{ url('/public') }}/js/jquery.min.js"></script>
    <script src="{{ url('/public') }}/js/jquery.validate.min.js"></script>
    <script src="{{ url('/public') }}/js/jquery-validate.bootstrap-tooltip.min.js"></script>
    <script src="{{ url('/public') }}/js/bootstrap.min.js"></script>

  </head>
  <body>
  
  <div class="bg_img">

  @include('layouts.header')

  @yield('content')

  @include('layouts.footer')
   
   </div>
  </body>
</html>