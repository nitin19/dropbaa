        @if(Auth::check())
        <?php
        $user = Auth::user();    
        $user_id =  Auth::id(); 
        ?>
        <!--header-after-login-->
        <header class="after_login">
            <!-- <div class="container-fluid"> -->
            <div class="container">
                <div class="col-sm-4 col-xs-4 logo_img">
                    <a href="{{ url('/') }}"><img src="{{ url('/public') }}/images/logo.png" class="img-responsive"></a>
                </div>
                <div class="col-sm-8 col-xs-8 userimg text-right">
                  <div class="col-sm-8">
                <ul class="before_login_menu">
                        <li class="menu_link"><a href="{{ route('searchlivestocks.index') }}">Livestockads</a></li>
                        <li class="menu_link"><a href="{{ route('searchland.index') }}">Landads</a></li>
                </ul>
                </div>
               <div class="col-sm-4">
                    <div class="dropdown menu_after_login">
                    @if($user->profile_picture!='')
                        <button class="btn btn-primary dropdown-toggle dropdown_menu" type="button" data-toggle="dropdown"><span class="user_name_title">Welcome, {{ Auth::user()->name }}</span><img src="{{ url('/public') }}/uploads/propic/thumbnail/{{ $user->profile_picture }}" class="img-circle">
                        <i class="fa fa-caret-down"></i></button>
                    @else
                    <button class="btn btn-primary dropdown-toggle dropdown_menu" type="button" data-toggle="dropdown"><span class="user_name_title">Welcome, {{ Auth::user()->name }}</span><img src="{{ url('/public') }}/images/dummy.jpg" class="img-circle">
                    <i class="fa fa-caret-down"></i></button>
                    @endif
                        <ul class="dropdown-menu dropdown_menu_list">
                                  @if($user->user_type == 'live_stock_owner' ) 
                                  <li><a href="{{ route('livestockads.index') }}">{{ Auth::user()->name }}</a></li>
                                  @else
                                  <li><a href="{{ route('landads.index') }}">{{ Auth::user()->name }}</a></li>
                                  @endif
                                  <li><a href="{{ route('users.edit', $user->id) }}">Edit Profile</a></li>
                                  <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                             {{ csrf_field() }}
                                         </form>
                                      </a>
                                  </li>
                        </ul>
                    </div>
                 </div>
                </div>
            </div>
        </header>
        <!--header-after-login-->
        @else

        <!--header-before-login-->
        <header class="before_login" style="display: block;">
            <div class="container">
                <div class="col-sm-4 logo_img">
                    <a href="{{ url('/') }}"><img src="{{ url('/public') }}/images/logo.png" class="img-responsive"></a>
                </div>
                <div class="col-sm-8 col-xs-8 userimg text-right">
                    <ul class="before_login_menu">
                        <li class="menu_link"><a href="{{ route('searchlivestocks.index') }}">Find Livestock</a></li>
                        <li class="menu_link"><a href="{{ route('searchland.index') }}">Find Land</a></li>
                        <li class="menu_link"><a href="{{ route('login') }}">Login</a></li>
                        <li class="menu_link"><a href="{{ route('register') }}">Register</a></li>
                    </ul>
                </div>
            </div>
        </header>

        <!--header-before-login-->

        @endif

<style>
.menu_after_login i{color:#000;}
.dropdown_menu_list{width:100%;}
</style>
