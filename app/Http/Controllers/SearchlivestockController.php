<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Livestockads;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;

class SearchlivestockController extends Controller
{    

    public function __construct()
    {
       // $this->middleware('auth');
    }

     public function index(Request $request)

    {
        try { 

    // $livestockads = Livestockads::->where('status', 1)->where('deleted', 0)->orderBy('id','DESC')->paginate(5);
    // $livestockads = DB::table('livestockads')->where('status', 1)->where('deleted', 0)->orderBy('id','DESC')->paginate(5);
            
            $keyword = '';
            $live_stock_type = '';
            $location = '';
            $price = '';
            $distance = '';
            $search='';

            if ($request->has('distance')) {
                $distance = $request->get('distance');
                $ip  = !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
                //echo $ip;
                $url = "http://freegeoip.net/json/$ip";
                $ch  = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                $data = curl_exec($ch);
                //echo $data;
                curl_close($ch);
                $result = array();
                if ($data) {
                    $result = json_decode($data);
                    $lat = $result->latitude;
                    //echo $lat;
                    $lng = $result->longitude;
                    //echo $lng;
                } else {
                      $lat = '';
                      $lon = '';
                }
                $search.="SELECT *,3956*2*ASIN(SQRT(POWER(SIN((".$lat." - `lst_latitude`)*pi()/180/2),2)+COS(".$lat." * pi()/180) *COS(`lst_latitude` * pi()/180)*POWER(SIN((".$lng." -`lst_longitude`)* pi()/180/2),2))) as distance FROM livestockads having distance < $distance ";

               //if($request->has('live_stock_type')) {
                  //  $search.="AND `lst_live_stock`='$request->get('live_stock_type')'";
               // }
               // if($request->has('location')) {
                 //   $search.="AND `lst_zipcode`='$request->has('location')'";
               // }
                $search.="ORDER BY distance";
                //echo $search;
                $query = \DB::select($search);
                $livestockads = $query; 
             

            } else {
              $query = DB::table('livestockads');

                if ($request->has('keyword')) {
                    $keyword = $request->get('keyword');
                    $query->where('lst_ad_title', 'LIKE', '%' . $request->get('keyword') . '%');
                    $query->orWhere('lst_description', 'LIKE', '%' . $request->get('keyword') . '%');
                    $query->orWhere('lst_per_head_per_week', 'LIKE', '%' . $request->get('keyword') . '%');
                    $query->orWhere('lst_address', 'LIKE', '%' . $request->get('keyword') . '%');
                    $query->orWhere('lst_zipcode', 'LIKE', '%' . $request->get('keyword') . '%');
                }

                if ($request->has('live_stock_type')) {
                    $live_stock_type = $request->get('live_stock_type');
                    $query->whereRaw("find_in_set('".$request->get('live_stock_type')."', lst_live_stock)");
                }

                if ($request->has('location')) {
                    $location = $request->get('location');
                    $query->where('lst_zipcode', '=', $request->get('location'));
                }

                /*if ($request->has('price')) {
                     $price = $request->get('price');
                     $query->where('lst_per_head_per_week', '=', $request->get('price'));
                } */    
            
                
                $query->where('status', 1);
                $query->where('deleted', 0);
                $query->orderBy('id','DESC');
                $livestockads = $query->paginate(10); 
            }   

            $agistmentcats = DB::table('agistmentcat')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->orderBy('id','DESC')
                            ->get();
            $landcats = DB::table('landcat')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->orderBy('id','DESC')
                            ->get();
            $sheepcats = DB::table('sheepcat')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->orderBy('id','DESC')
                            ->get();

        return view('searchlivestocks.index',compact('livestockads', 'agistmentcats', 'landcats', 'sheepcats', 'keyword', 'live_stock_type', 'location', 'price', 'distance'));
           // ->with('i', ($request->input('page', 1) - 1) * 5);

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        } catch (\Exception $e) {
            $errorClass = new ErrorClass();
            $errors = $errorClass->saveErrors($e);
        }     

    }


    
     public function create()

    {

        //
    }
    
        public function store(Request $request)

    {

        //
    }
    
        public function show($id)

    {

        try {

        $livestockad = Livestockads::find($id);

        $user = DB::table('users')->where('id', $livestockad->lst_user_id)->first();

        $postgalleries = DB::table('postgallery')
                            ->where('post_id', $id)
                            ->where('post_type', '=', 'livestockads')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->orderBy('id','DESC')
                            ->get();


        return view('searchlivestocks.show', compact('livestockad', 'postgalleries', 'user'));

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorClass();
            $errors = $errorClass->saveErrors($e);
       } 

    }
    
        public function edit($id)

    {

       //

    }
    
        public function update(Request $request, $id)

    {

        //
        

    }
    
        public function destroy($id)

    {

        //

    }
}
