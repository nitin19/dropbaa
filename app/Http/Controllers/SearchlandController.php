<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
//use Auth;
use App\Landads;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;

class SearchlandController extends Controller
{    

    public function __construct()
    {
       // $this->middleware('auth');
    }

     public function index(Request $request)

    {
        try { 

            
            $keyword = '';
            $land_type = '';
            $live_type = '';
            $location = '';
            $price = '';
            $distance = '';
            $search_data = array();

             $search='';

            if ($request->has('distance')!='') {
                $distance = $request->get('distance');
                //$latitude = $request->get('latitude');
                //$langitude = $request->get('langitude');
                $ip  = !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
                $url = "http://freegeoip.net/json/$ip";
                $ch  = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                $data = curl_exec($ch);
                //echo $data;
                curl_close($ch);
                $result = array();
                if ($data) {
                    $result = json_decode($data);
                    $lat = $result->latitude;
 
                    $lng = $result->longitude;

                } else {
                      $lat = '';
                      $lng = '';
                  }
                  //$landads = Landads::select(
                           //  DB::raw("*,
                           //       ( 6371 * acos( cos( radians(lnd_latitude) ) *
                            //    cos( radians( ".$lat." ) )
                            //    * cos( radians( ".$lng." ) - radians(lnd_longitude)
                            //    ) + sin( radians(lnd_latitude) ) *
                            //    sin( radians( ".$lat." ) ) )
                            //  ) AS distance"))
                        // ->having("distance", "<", "$distance")
                        // ->orderBy("distance")
                        // ->get();
                        

               $search.="SELECT *,3956*2*ASIN(SQRT(POWER(SIN((".$lat." - `lnd_latitude`)*pi()/180/2),2)+COS(".$lat." * pi()/180) *COS(`lnd_latitude` * pi()/180)*POWER(SIN((".$lng." -`lnd_longitude`)* pi()/180/2),2))) as distance FROM landads having distance < $distance ";

            //if($request->has('land_type')!='') {
                //$land_type = $request->get('land_type');
                //$search.="AND `lnd_land_type`='$request->get('live_type')'";
            //}
            //if($request->has('live_type')!='') {
               // $live_type = $request->get('live_type');
               // $search.="AND `lnd_i_want`='$request->get('live_type').'";
           // }
           // if($request->has('location')!='') {
              //  $location = $request->get('location');
               // $search.="AND `lnd_zipcode`='$request->has('location')'";
           // }

             $search.=" ORDER BY distance";
             //echo $search;
             $query = \DB::select($search);
            // echo '<pre>';
            // print_r($query);
             //echo '</pre>';
             $landads = $query; 
             

            } else {

              $query = DB::table('landads');

                if ($request->has('keyword')) {
                    $keyword = $request->get('keyword');
                    $query->where('lnd_ad_title', 'LIKE', '%' . $request->get('keyword') . '%');
                    $query->orWhere('lnd_description', 'LIKE', '%' . $request->get('keyword') . '%');
                    $query->orWhere('lnd_per_head_per_week', 'LIKE', '%' . $request->get('keyword') . '%');
                    $query->orWhere('lnd_address', 'LIKE', '%' . $request->get('keyword') . '%');
                    $query->orWhere('lnd_zipcode', 'LIKE', '%' . $request->get('keyword') . '%');
                }

                if ($request->has('land_type')) {
                         
                    $land_type = $request->get('land_type');
                    $query->where('lnd_land_type', '=', $request->get('land_type'));
                   // $query->whereRaw("find_in_set('".$request->get('land_type')."', lnd_land_type)");
                }
                
                if ($request->has('live_type')) {
                    $live_type = $request->get('live_type');
                    $query->where('lnd_i_want', '=', $request->get('live_type'));
                   // $query->whereRaw("find_in_set('".$request->get('land_type')."', lnd_land_type)");
                }

                if ($request->has('location')) {
                    $location = $request->get('location');
                    $query->where('lnd_zipcode', '=', $request->get('location'));
                }
                $query->where('status', 1);
                $query->where('deleted', 0);
                $query->orderBy('id','DESC');
                $landads = $query->paginate(10);   
            }


            $agistmentcats = DB::table('agistmentcat')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->orderBy('id','DESC')
                            ->get();
            $landcats = DB::table('landcat')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->orderBy('id','DESC')
                            ->get();
            $sheepcats = DB::table('sheepcat')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->orderBy('id','DESC')
                            ->get();

        return view('searchland.index',compact('landads', 'agistmentcats', 'landcats', 'sheepcats', 'keyword', 'land_type', 'live_type', 'location', 'price', 'distance'));
            //->with('i', ($request->input('page', 1) - 1) * 5);

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        } catch (\Exception $e) {
            $errorClass = new ErrorClass();
            $errors = $errorClass->saveErrors($e);
        }     

    }


    
     public function create()

    {

        //
    }
    
        public function store(Request $request)

    {

        //
    }
    
    public function show($id)
    {
       try { 
        $postgalleries = DB::table('postgallery')
                            ->where('post_id', $id)
                            ->where('post_type', '=', 'landads')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->orderBy('id','DESC')
                            ->get();

        $landad = Landads::find($id);

        $user = DB::table('users')->where('id', $landad->lnd_user_id)->first();

        return view('searchland.show',compact('landad', 'postgalleries', 'user'));
        
        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorClass();
            $errors = $errorClass->saveErrors($e);
       }

    }
    
        public function edit($id)

    {

       //

    }
    
        public function update(Request $request, $id)

    {

        //
        

    }
    
        public function destroy($id)

    {

        //

    }
}
