<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;

class UserController extends Controller
{    

    public function __construct()
    {
        $this->middleware('auth');
    }

     public function index(Request $request)

    {
          

    }
    
     public function create()

    {


    }
    
        public function store(Request $request)

    {

    }
    
        public function show($id)

    {

        
    }
    
        public function edit($id)

    {
        $user = User::find($id);
        return view('users.edit', compact('user'));

    }
    
   public function update(Request $request, $id)

    {

        $user = User::findOrFail($id); 
      
        $name = $request->name;
        $email = $request->email;
        $phone = $request->phone;
        $address_line_1 = $request->address_line_1;
        $address_line_2 = $request->address_line_2;
        $description = $request->description;
        $updated_by = $id;

        $input = $request->only(['name', 'email', 'phone', 'address_line_1', 'address_line_2', 'description','updated_by']); 

        $updated =  $user->fill($input)->save();

        $file = $_FILES['profile_picture']['name'];

        if($file!=''){

          $file = Input::file('profile_picture')->getFilename();
          $thumbnail_path = public_path('uploads/propic/thumbnail/');
          $original_path = public_path('uploads/propic/original/');
          $file_name = 'user_'. $user->id .'_'. time() . '.' . Input::file('profile_picture')->getClientOriginalExtension();
          
            Image::make(Input::file('profile_picture')->getRealPath())
                  ->resize(261,null,function ($constraint) {
                    $constraint->aspectRatio();
                     })
                  ->save($original_path . $file_name)
                  ->resize(90, 90)
                  ->save($thumbnail_path . $file_name);

            $user->update(['profile_picture' => $file_name]);

          } 
       
        if($updated) {

            if($user->user_type == 'live_stock_owner') {
                return redirect()->route('livestockads.index')

                ->with('success','User successfully edited!');
            } else {
                return redirect()->route('landads.index')

                ->with('success','User successfully edited!');
            }

        } else {

          if($user->user_type == 'live_stock_owner') {
                return redirect()->route('livestockads.index')

                ->with('error','Woops Something is wrong!');
            } else {
                return redirect()->route('landads.index')

                ->with('error','Woops Something is wrong!');
            }

         /*return redirect()->route('users.index')
                ->with('error','Woops Something is wrong!');    
            */
        }

    }
       
}
