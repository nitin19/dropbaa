<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Livestockads;
use App\Post_gallery;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;

class LivestockadsController extends Controller
{    

	public function __construct()
    {
        $this->middleware('auth');
    }

     public function index(Request $request)

    {   
        $user_id =  Auth::id();
        try { 

            /**** query data print *********/

                //$lastQuery = end($livestockads_user);

                //print_r($lastQuery);

            /****query data print end*****/

            $livestockads = DB::table('livestockads')
            ->where('lst_user_id', $user_id)
            ->where('status', '=', 1)
            ->where('deleted', '=', 0)
            ->orderBy('id','DESC')->paginate(10);




        return view('livestockads.index',compact('livestockads', 'livestockads_user'))

            ->with('i', ($request->input('page', 1) - 1) * 5);

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorClass();
            $errors = $errorClass->saveErrors($e);

        } 

    }
	
	 public function create()

    {   
        try {  

    	$agistmentcats = DB::table('agistmentcat')
    						->where('status', '=', 1)
    						->where('deleted', '=', 0)
    						->orderBy('id','DESC')
    						->get();
    	$landcats = DB::table('landcat')
    						->where('status', '=', 1)
    						->where('deleted', '=', 0)
    						->orderBy('id','DESC')
    						->get();
    	$sheepcats = DB::table('sheepcat')
    						->where('status', '=', 1)
    						->where('deleted', '=', 0)
    						->orderBy('id','DESC')
    						->get();

        return view('livestockads.create',compact('agistmentcats', 'landcats', 'sheepcats'));

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorClass();
            $errors = $errorClass->saveErrors($e);
       } 

    }
	
	    public function store(Request $request)

    {

        try {  

       /* $this->validate($request, array(

            'lst_agistment_type' => 'required|max:190',

            'lst_ad_title' => 'required|max:190',

            'lst_live_stock' => 'required|max:190',

            'lst_supply_fencing' => 'required|max:190',

            'lst_payment' => 'required|max:190',

            'lst_per_head_per_week' => 'required|max:9',

            'lst_description' => 'required|max:5000',

        ));*/
		
		$livestockad = new Livestockads;

		if(Auth::check()){
     		$user = Auth::user();    
            $user_id =  Auth::id();
		 }

		$livestockad->lst_user_id = Auth::id();
		$livestockad->lst_agistment_type = $request->lst_agistment_type;
		$livestockad->lst_ad_title = $request->lst_ad_title;
		$livestockad->lst_live_stock = implode(',',$request->lst_live_stock);
		$livestockad->lst_supply_fencing = $request->lst_supply_fencing;
		$livestockad->lst_payment = $request->lst_payment;
		$livestockad->lst_per_head_per_week = $request->lst_per_head_per_week;
        $livestockad->lst_address = $request->lst_address;
        $livestockad->lst_zipcode = $request->lst_zipcode;
		$livestockad->lst_description = $request->lst_description;
        $livestockad->created_by = Auth::id();
        $livestockad->lst_latitude = $request->lst_latitude;
        $livestockad->lst_longitude = $request->lst_longitude;
        $livestockad->save();

        $insertedId = $livestockad->id;

        if($request->hasfile('lst_pictures'))
         {
            foreach($request->file('lst_pictures') as $image )
            {
                $name = 'lndad_'. Auth::id() .'_'. time() . '.' . $image->getClientOriginalName();
                $image->move(public_path('uploads/livestockadspics/'), $name); 

                $date = date('Y-m-d H:i:s');
                
                DB::table('postgallery')->insert(
                    ['post_id' => $insertedId, 'post_type' => 'livestockads', 'file_name' => $name, 'created_by' => $user_id, 'created_at' => $date]
                    );
            }
         }

         return redirect()->route('livestockads.index')
                        ->with('success','Post created successfully');    

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }    
		             
    }
	
	public function show($id)

    {
        try {

        $livestockad = Livestockads::find($id);

        $postgalleries = DB::table('postgallery')
                            ->where('post_id', $id)
                            ->where('post_type', '=', 'livestockads')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->orderBy('id','DESC')
                            ->get();


        return view('livestockads.show',compact('livestockad', 'postgalleries'));

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorClass();
            $errors = $errorClass->saveErrors($e);
       } 
    }
	
	public function edit($id)
    {
        try { 

        $livestockad = Livestockads::find($id);

        $postgalleries = DB::table('postgallery') 
                            ->where('post_id', $id) 
                            ->where('post_type', '=', 'livestockads') 
                            ->where('status', '=', 1) 
                            ->where('deleted', '=', 0) 
                            ->orderBy('id','DESC') 
                            ->get();

        $agistmentcats = DB::table('agistmentcat')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->orderBy('id','DESC')
                            ->get();

    /*	$livestockscats = DB::table('livestockscats')
                       ->where('status', '=', 1)
                       ->where('deleted', '=', 0)
                       ->orderBy('id','DESC')
                       ->get(); */

    	$sheepcats = DB::table('sheepcat')
                        ->where('status', '=', 1)
                        ->where('deleted', '=', 0)
                        ->orderBy('id','DESC')
                        ->get();
        return view('livestockads.edit',compact('livestockad','agistmentcats', 'sheepcats', 'postgalleries'));

       } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorClass();
            $errors = $errorClass->saveErrors($e);
        } 

    }
	
	    public function update(Request $request, $id)
    {
        

     try{
        if(Auth::check()){
            $user = Auth::user();    
            $user_id =  Auth::id();
        }
       
        $lst_agistment_type = $request->lst_agistment_type;
        $lst_ad_title = $request->lst_ad_title;
        $lst_live_stock = implode(',',$request->lst_live_stock);
        $lst_supply_fencing = $request->lst_supply_fencing;
        $lst_payment = $request->lst_payment;
        $lst_per_head_per_week = $request->lst_per_head_per_week;
        $lst_address = $request->lst_address;
        $lst_zipcode = $request->lst_zipcode;
        $lst_description = $request->lst_description;
        $updated_by = Auth::id();
        $lst_latitude = $request->lst_latitude;
        $lst_longitude = $request->lst_longitude;

         $upQry = DB::table('livestockads')
                ->where('id', $id)
                ->update(['lst_agistment_type' => $lst_agistment_type, 'lst_ad_title' => $lst_ad_title, 'lst_live_stock' => $lst_live_stock, 'lst_supply_fencing' => $lst_supply_fencing, 'lst_payment' => $lst_payment, 'lst_per_head_per_week' => $lst_per_head_per_week, 'lst_description' => $lst_description, 'lst_address' => $lst_address, 'lst_zipcode' => $lst_zipcode, 'lst_latitude' => $lst_latitude, 'lst_longitude' => $lst_longitude, 'updated_by' => $updated_by]);


        if($request->hasfile('lst_pictures'))
         {
            foreach($request->file('lst_pictures') as $image )
            {
                $name = 'lndad_'. Auth::id() .'_'. time() . '.' . $image->getClientOriginalName();
                $image->move(public_path('uploads/livestockadspics/'), $name); 

                $date = date('Y-m-d H:i:s');
                
                DB::table('postgallery')->insert(
                    ['post_id' => $id, 'post_type' => 'livestockads', 'file_name' => $name, 'updated_by' => $user_id, 'updated_at' => $date]
                    );
            }
         }

         return redirect()->route('livestockads.index')

                        ->with('success','Post updated successfully');
		
    } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

    } catch (\Exception $e) {
            $errorClass = new ErrorClass();
            $errors = $errorClass->saveErrors($e);
    } 

    }
	
	 public function destroy($id)
    {
        try
        {

            $livestockad = Livestockads::findOrFail($id); 

            $deleted = $livestockad->delete();

            if($deleted) {

                 return redirect()->route('livestockads.index')

                 ->with('success','User successfully deleted!');
            } else {

                 return redirect()->route('livestockads.index')

                 ->with('error','Woops Something is wrong!'); 
            }
        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorClass();
            $errors = $errorClass->saveErrors($e);
        } 
    }
     public static function delete_image(Request $request)
     {       
        //$galleryid=$_GET['galleryid'];
        $galleryid = $request->galleryid;

         $delquery = DB::table('postgallery')->where('id', $galleryid)->delete();

        if($delquery) {

           echo 'Success';

         } else {

            echo 'Fail';
          }
        
    }
}
