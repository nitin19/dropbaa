<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;

class ForgetpasswordController extends Controller
{    

    public function __construct()
    {
        
    }

    public function index()

    {
    	return view('forgetpassword.index');
    }
    public function send_password()
    {

        $email = trim($_POST['email']); 
        $userInfo = DB::table('users')->where(array('email' => $email, 'deleted' => 0))->first();
        $userInfoCount = count($userInfo);
            if($userInfoCount > 0 ){
            	//$mainBusinessInfo = DB::table('business_settings')->where('is_main_business', '1')->first();
               // $main_business_name = $mainBusinessInfo->main_business_name;
               // $main_business_email = $mainBusinessInfo->main_business_email;
        		$to = $email;
                        $subject = "Forgot Password";
                        $message = '';
                        $message .= '<html><body>';
                        $message .= "Hello ".$userInfo->name.",<br />";
                        $message .=  "<br />";
                        $message .= "<b> Greeting of the day.</b><br />";
                        $message .=  "<br />";
                        $message .= "<b> Your request for forgot password has been accepted.</b><br />";
                        $message .= "<b> Here is your login details: </b> <br />";
                        $message .= "<b> User Name : </b> ".$userInfo->email."<br />";
                        $message .= "<b> Password: </b> ".$userInfo->hdpwd."<br />";
                        $message .=  "<br />";
                        $message .= "<br /><br />Thanks & best regards<br />";
                        //$message .= "<b> ".$main_business_name."</b><br />";
                        $message .= "</font>";
                        $message .= "</body></html>";
                        $headers = "MIME-Version: 1.0" . "\r\n";
                        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                        $headers .= 'From: 'Dropbaa'<info@dropbaa.net>' . "\r\n";
                        $sendMail = mail($to,$subject,$message,$headers);
                        if($sendMail) {
            return redirect()->back()->with('success','Your password has been sent to your email. Please check your email.');     } else {
            return redirect()->back()->with('error','We can not find a user with that e-mail address.');
                        }
            return redirect()->back()->with('success','Your password has been sent to your email. Please check your email.');
        } else {
			return redirect()->back()->with('error','We can not find a user with that e-mail address.');
        }
        
     }      
     
}
