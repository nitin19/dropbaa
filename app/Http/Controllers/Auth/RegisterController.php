<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use Session;
use DB;
use Hash;
use Image;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'phone' => 'required|string|max:255',
            'address_line_1' => 'required|string|max:255',
            'user_type' => 'required|string|max:255',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'hdpwd' => $data['password'],
            'phone' => $data['phone'],
            'zipcode' => $data['zipcode'],
            'address_line_1' => $data['address_line_1'],
            'address_line_2' => $data['address_line_2'],
            'description' => $data['description'],
            'user_type' => $data['user_type'],
        ]);

                $file = $_FILES['profile_picture']['name'];

        if($file!=''){

          $file = Input::file('profile_picture')->getFilename();
          $thumbnail_path = public_path('uploads/propic/thumbnail/');
          $original_path = public_path('uploads/propic/original/');
          $file_name = 'user_'. $user->id .'_'. time() . '.' . Input::file('profile_picture')->getClientOriginalExtension();
          
            Image::make(Input::file('profile_picture')->getRealPath())
                  ->resize(261,null,function ($constraint) {
                    $constraint->aspectRatio();
                     })
                  ->save($original_path . $file_name)
                  ->resize(90, 90)
                  ->save($thumbnail_path . $file_name);

            $user->update(['profile_picture' => $file_name]);

          } else {
             }

        if(trim($data['user_type'])!='') {
            if($data['user_type']=='land_owner') {
                $this->redirectTo = '/landads/create';
            } else {
                $this->redirectTo = '/livestockads/create';
            }
        }

          return $user;
    }
}
