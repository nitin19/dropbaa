<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated($request, $user)
    {   

        if($user->is_admin == 1) {
             echo 'I am Admin';
            return redirect()->intended('/admin_path_here');
        } else {

           if($user->deleted == 1) {

                Auth::logout();
                 return redirect()->intended('/login')->with('error','Your account is deleted. Please contact with website admin for more information');

           } else {

            if($user->status == 1){
               
                if($user->user_type == 'live_stock_owner') {
                     return redirect()->intended('/livestockads');
                     } else {
                     return redirect()->intended('/landads');
                   }

            } else {
              Auth::logout();
                 return redirect()->intended('/login')->with('error','Your account is not active. Please contact with website admin to activate you account');   
            }

           } 
        }

       // return redirect()->intended('/path_for_normal_user');
    }
}
