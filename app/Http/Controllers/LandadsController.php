<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Landads;
use App\Postgallery;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;

class LandadsController extends Controller
{    

    public function __construct()
    {
        $this->middleware('auth');
    }

     public function index(Request $request)

    {
         //return view('landads.index');
        $user_id =  Auth::id();
        try { 

            $landads = DB::table('landads')
            ->where('lnd_user_id', $user_id)
            ->where('status', '=', 1)
            ->where('deleted', '=', 0)
            ->orderBy('id','DESC')->paginate(10);

        return view('landads.index',compact('landads'))

            ->with('i', ($request->input('page', 1) - 1) * 5);

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

              // return $e->getMessage();
              // echo  $e->getMessage(); exit;

        } catch (\Exception $e) {
            $errorClass = new ErrorClass();
            $errors = $errorClass->saveErrors($e);
                
              // return $e->getMessage();
              // echo  $e->getMessage(); exit;
        }     

    }
    
     public function create()

    {

        try {  

        $agistmentcats = DB::table('agistmentcat')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->orderBy('id','DESC')
                            ->get();
        $landcats = DB::table('landcat')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->orderBy('id','DESC')
                            ->get();
        $sheepcats = DB::table('sheepcat')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->orderBy('id','DESC')
                            ->get();
        return view('landads.create',compact('agistmentcats', 'landcats', 'sheepcats'));

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorClass();
            $errors = $errorClass->saveErrors($e);
       } 

    }
    
    public function store(Request $request)

    {

        try {

        if(Auth::check()){
            $user = Auth::user();    
            $user_id =  Auth::id();
         }
        
        $landad = new Landads;
        $landad->lnd_user_id = Auth::id();
        $landad->lnd_agistment_type = $request->lnd_agistment_type;
        $landad->lnd_ad_title = $request->lnd_ad_title;
        $landad->lnd_land_type = $request->lnd_land_type;
        $landad->lnd_i_want = implode(',',$request->lnd_i_want);
        $landad->lnd_payment = $request->lnd_payment;
        $landad->lnd_in_hectares = $request->lnd_in_hectares;
        $landad->lnd_per_head_per_week = $request->lnd_per_head_per_week;
        $landad->lnd_description = $request->lnd_description;
        $landad->lnd_address = $request->lnd_address;
        $landad->lnd_zipcode = $request->lnd_zipcode;
        $landad->created_by = Auth::id();
        $landad->lnd_latitude = $request->lnd_latitude; 
        $landad->lnd_longitude = $request->lnd_longitude;

/*        $zip = $request->lnd_zipcode;
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($zip)."&sensor=false";
        $result_string = file_get_contents($url);
        $result = json_decode($result_string, true);
        $landad->lnd_latitude = $result['results'][0]['geometry']['location']['lat'];
        $landad->lnd_longitude = $result['results'][0]['geometry']['location']['lng'];*/

        
        $landad->save();

        $insertedId = $landad->id;

        if($request->hasfile('lnd_pictures'))
         {
            foreach($request->file('lnd_pictures') as $image )
            {
                $name = 'lndad_'. Auth::id() .'_'. time() . '.' . $image->getClientOriginalName();
                $image->move(public_path('uploads/landadspics/'), $name); 

                $date = date('Y-m-d H:i:s');
                
                DB::table('postgallery')->insert(
                    ['post_id' => $insertedId, 'post_type' => 'landads', 'file_name' => $name, 'created_by' => $user_id, 'created_at' => $date]
                );
            }
         }

     //      


        return redirect()->route('landads.index')

                        ->with('success','Post created successfully');

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorClass();
            $errors = $errorClass->saveErrors($e);
       }                   

    }
    
        public function show($id)

    {

        try { 
        $postgalleries = DB::table('postgallery')
                            ->where('post_id', $id)
                            ->where('post_type', '=', 'landads')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->orderBy('id','DESC')
                            ->get();

        $landad = Landads::find($id);

        return view('landads.show',compact('landad', 'postgalleries'));
        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorClass();
            $errors = $errorClass->saveErrors($e);
       }
    }
    
        public function edit($id)

    {

        try { 
        $postgalleries = DB::table('postgallery')
                            ->where('post_id', $id)
                            ->where('post_type', '=', 'landads')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->orderBy('id','DESC')
                            ->get(); 

        $agistmentcats = DB::table('agistmentcat')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->orderBy('id','DESC')
                            ->get();
        $landcats = DB::table('landcat')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->orderBy('id','DESC')
                            ->get();
        $sheepcats = DB::table('sheepcat')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->orderBy('id','DESC')
                            ->get();
        $landad = Landads::find($id);
        return view('landads.edit',compact('agistmentcats', 'landcats', 'sheepcats', 'landad', 'postgalleries'));

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorClass();
            $errors = $errorClass->saveErrors($e);
       } 


    }
    
    public function update(Request $request, $id)

    {
        try {
        
      if(Auth::check()){
            $user = Auth::user();    
            $user_id =  Auth::id();
         }

        $lnd_agistment_type = $request->lnd_agistment_type;
        $lnd_ad_title = $request->lnd_ad_title;
        $lnd_land_type = $request->lnd_land_type;
        $lnd_i_want = implode(',',$request->lnd_i_want);
        $lnd_payment = $request->lnd_payment;
        $lnd_in_hectares = $request->lnd_in_hectares;
        $lnd_per_head_per_week = $request->lnd_per_head_per_week;
        $lnd_description = $request->lnd_description;
        $lnd_address = $request->lnd_address;
        $lnd_zipcode = $request->lnd_zipcode;
        $updated_by = Auth::id();
        $lnd_latitude = $request->lnd_latitude; 
        $lnd_longitude = $request->lnd_longitude;

        $upQry = DB::table('landads')
                ->where('id', $id)
                ->update(['lnd_agistment_type' => $lnd_agistment_type, 'lnd_ad_title' => $lnd_ad_title, 'lnd_land_type' => $lnd_land_type, 'lnd_i_want' => $lnd_i_want, 'lnd_payment' => $lnd_payment, 'lnd_in_hectares' => $lnd_in_hectares, 'lnd_per_head_per_week' => $lnd_per_head_per_week, 'lnd_description' => $lnd_description, 'lnd_address' => $lnd_address, 'lnd_zipcode' => $lnd_zipcode, 'lnd_latitude' => $lnd_latitude, 'lnd_longitude' => $lnd_longitude, 'updated_by' => $updated_by]);

          
        if($request->hasfile('lnd_pictures'))
         {
            foreach($request->file('lnd_pictures') as $image )
            {
                $name = 'lndad_'. Auth::id() .'_'. time() . '.' . $image->getClientOriginalName();
                $image->move(public_path('uploads/landadspics/'), $name); 

                $date = date('Y-m-d H:i:s');
                
                DB::table('postgallery')->insert(
                    ['post_id' => $id, 'post_type' => 'landads', 'file_name' => $name, 'updated_by' => $user_id, 'updated_at' => $date]
                    );
            }
         }

        return redirect()->route('landads.index')

                        ->with('success','Post updated successfully');

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorClass();
            $errors = $errorClass->saveErrors($e);
       }

    }
    
    /*    public function destroy($id)

    {

        Landads::find($id)->delete();

        return redirect()->route('landads.index')

                        ->with('success','Post deleted successfully');

    }*/
    public function destroy($id)
    {
        try
        {
            $landads = Landads::findOrFail($id); 

            $deleted = $landads->delete();

            if($deleted) {

                 return redirect()->route('landads.index')

                 ->with('success','User successfully deleted!');
            } else {

                 return redirect()->route('landads.index')

                 ->with('error','Woops Something is wrong!'); 
            }
        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorClass();
            $errors = $errorClass->saveErrors($e);
        } 
    }
    public static function delete_image(Request $request)
     {       
        //$galleryid=$_GET['galleryid'];
        $galleryid = $request->galleryid;

         $delquery = DB::table('postgallery')->where('id', $galleryid)->delete();

        if($delquery) {

           echo 'Success';

         } else {

            echo 'Fail';
          }
        
    }
}
