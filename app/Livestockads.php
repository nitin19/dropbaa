<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Livestockads extends Model
{
   				public $fillable = [
   				 'lst_agistment_type','lst_ad_title','lst_live_stock','lst_supply_fencing','lst_payment','lst_per_head_per_week','lst_address','lst_zipcode','lst_latitude','lst_longitude','lst_pictures','lst_description',
   				];
}
