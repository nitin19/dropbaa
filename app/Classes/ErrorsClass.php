<?php
namespace App\Classes;
use DB;
use Auth;
class ErrorsClass
{

    protected $table;
    protected $connection;

    public function __construct()
    {
        $this->table      = env('DB_LOG_TABLE', 'logs');
        $this->connection = env('DB_LOG_CONNECTION', env('DB_CONNECTION', 'mysql'));
    }  

    public function getErrors() {
        return false;
       }

    public function saveErrors($record) {

        $data = [
            'instance'    => gethostname(),
            'message'     => $record->getTraceAsString() ."\n",
            'channel'     => 'local',
            'level'       => '400',
            'level_name'  => 'DBQRYERROR',
            'context'     => '[]',
            'remote_addr' => isset($_SERVER['REMOTE_ADDR'])     ? ip2long($_SERVER['REMOTE_ADDR']) : null,
            'user_agent'  => isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT']      : null,
            'created_by'  => Auth::id() > 0 ? Auth::id() : null,
            'created_at'  => date('Y-m-d H:i:s')
        ];

        DB::connection($this->connection)->table($this->table)->insert($data);
    }

}
