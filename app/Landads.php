<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Landads extends Model
{
 				public $fillable = [
 					'lnd_agistment_type','lnd_ad_title','lnd_land_type','lnd_i_want','lnd_in_hectares','lnd_per_head_per_week','lnd_pictures','lnd_description','lnd_address','lnd_zipcode','lnd_longitude','lnd_latitude',
 				];
}
