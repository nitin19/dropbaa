<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postgallery extends Model
{
 				public $fillable = [
 					'post_id','post_type','file_name','created_by','updated_by',
 				];
}

