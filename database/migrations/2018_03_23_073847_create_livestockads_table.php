<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLivestockadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('livestockads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lst_user_id');
            $table->string('lst_agistment_type');
            $table->text('lst_ad_title');
            $table->string('lst_live_stock');
            $table->string('lst_supply_fencing');
            $table->string('lst_payment');
            $table->string('lst_per_head_per_week');
            $table->text('lst_pictures');
            $table->longText('lst_description');
            $table->Text('lst_address');
            $table->string('lst_zipcode');
            $table->string('lst_latitude');
            $table->string('lst_longitude');
            $table->integer('status');
            $table->integer('deleted');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('livestockads');
    }
}
