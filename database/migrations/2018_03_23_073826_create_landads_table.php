<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lnd_user_id');
            $table->string('lnd_agistment_type');
            $table->text('lnd_ad_title');
            $table->string('lnd_land_type');
            $table->string('lnd_i_want');
            $table->string('lnd_in_hectares');
            $table->string('lnd_per_head_per_week');
            $table->longText('lnd_description');
            $table->Text('lnd_address');
            $table->string('lnd_zipcode');
            $table->string('lnd_longitude');
            $table->string('lnd_latitude');
            $table->integer('status');
            $table->integer('deleted');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('landads');
    }
}
