<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('/landads','LandadsController');
Route::resource('/livestockads','LivestockadsController');
Route::resource('/searchlivestocks', 'SearchlivestockController');
Route::resource('/searchland', 'SearchlandController');
Route::get('/livestockads/{id}/edit/delete_image/{galleryid}','LivestockadsController@delete_image');
Route::get('/landads/{id}/edit/delete_image/{galleryid}','LandadsController@delete_image');
Route::resource('/users','UserController');
Route::get('/forgetpassword', 'ForgetpasswordController@index');
Route::post('/forgetpassword/send_password', 'ForgetpasswordController@send_password');

//Route::resource('/forgetpass','ForgetpasswordController');
//Route::get('/subscription/{user_id}', 'SubscriptionController@subscription');
//Route::get('/edit/{user_id}', 'UserController@edit');
//Route::get('editprofile', 'ProfileController@edit')->name('editprofile');
// Route::get('editprofile', function() {
//     return view('pages.editprofile');
// })->name('editprofile');



